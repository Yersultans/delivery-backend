import {
  type,
  project_id,
  private_key_id,
  private_key,
  client_email,
  client_id,
  auth_uri,
  token_uri,
  auth_provider_x509_cert_url,
  client_x509_cert_url
} from '../firebaseCredentials.json'

export default {
  type: type,
  project_id: project_id,
  private_key_id: private_key_id,
  private_key: private_key
    .replace(new RegExp('\\\\n', 'g'), '\n')
    .replace('"', ''),
  client_email: client_email,
  client_id: client_id,
  auth_uri: auth_uri,
  token_uri: token_uri,
  auth_provider_x509_cert_url: auth_provider_x509_cert_url,
  client_x509_cert_url: client_x509_cert_url
}
