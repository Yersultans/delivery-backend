import dotenv from 'dotenv'

if (process.env.NODE_ENV !== 'production') dotenv.config()

export const { MONGO_URL } = process.env
export const { PASSPORT_KEY } = process.env
export const { AllowedUrls } = process.env
export const { NODE_ENV } = process.env
export const { FIREBASE_STORAGE_BUCKET } = process.env
export const { CLIENT_PATH } = process.env

export const { SERVER_URL } = process.env
