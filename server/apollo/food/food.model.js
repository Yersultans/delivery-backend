const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const FoodSchema = new Schema(
  {
    name: String,
    description: String,
    price: Number,
    category: String,
    imageUrl: String,
    proteins: Number,
    fats: Number,
    carbohydrates: Number,
    calories: Number,
    isVegan: Boolean,
    isActive: Boolean,
    isDeleted: Boolean,
    restaurant: { type: Schema.Types.ObjectId, ref: 'Restaurant' }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

FoodSchema.plugin(mongoosePaginate)

const Food = mongoose.model('Food', FoodSchema)
module.exports = Food
