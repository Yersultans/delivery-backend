import model from './food.model'
import gqlLoader from '../gqlLoader'
import resolvers from './food.resolvers'
import loader from './food.loader'

module.exports = {
  typeDefs: gqlLoader('./food/food.graphql'),
  model,
  resolvers,
  loader
}
