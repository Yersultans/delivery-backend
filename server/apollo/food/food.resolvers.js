import { authenticated, authorized } from '../auth/auth.helper'

const hasCategory = ({ categories, foodCategory }) => {
  let isCategory = false
  categories.forEach(category => {
    if (category === foodCategory) {
      isCategory = true
    }
  })
  return isCategory
}

module.exports = {
  Query: {
    async foods(_, args, ctx) {
      const items = await ctx.models.Food.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async foodsByRestaurant(_, { restaurantId }, ctx) {
      const items = await ctx.models.Food.find({
        restaurant: restaurantId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async food(_, args, ctx) {
      const item = await ctx.models.Food.findById(args.id).exec()
      if (!item) {
        throw new Error('Food does not exist')
      }
      return item
    }
  },
  Mutation: {
    async addFood(_, { input }, ctx) {
      const item = new ctx.models.Food(input)
      const { category, restaurant: restaurantId } = input
      const restaurant = await ctx.models.Restaurant.findById(restaurantId)
      if (
        !hasCategory({
          categories: restaurant.categories,
          foodCategory: category
        })
      ) {
        restaurant.categories = [...restaurant.categories, category]
        await restaurant.save()
      }
      await item.save()
      return item
    },
    async updateFood(_, { id, input }, ctx) {
      const item = await ctx.models.Food.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('Food not found')
      }
      const food = await ctx.models.Food.findById(id)
      const { category, restaurant: restaurantId } = food
      const restaurant = await ctx.models.Restaurant.findById(restaurantId)
      if (
        !hasCategory({
          categories: restaurant.categories,
          foodCategory: category
        })
      ) {
        restaurant.categories = [...restaurant.categories, category]
        await restaurant.save()
      }
      return item
    },
    async deleteFood(_, { id }, ctx) {
      const result = await ctx.models.Food.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )
      const food = await ctx.models.Food.findById(id)
      const { category, restaurant: restaurantId } = food
      const foodsByRestaurant = await ctx.models.Food.find({
        restaurant: restaurantId,
        category: category,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      if (foodsByRestaurant.length === 0) {
        restaurant.categories = restaurant.categories.filter(
          categoryData => categoryData !== category
        )
        await restaurant.save()
      }

      return id
    }
  },
  Food: {
    id(food) {
      return `${food._id}`
    },
    async restaurant(food, _, ctx) {
      const restaurantId = food.restaurant
      if (restaurantId) {
        const restaurant = await ctx.loaders.restaurantLoader.load(restaurantId)
        return restaurant
      }
      return null
    }
  }
}
