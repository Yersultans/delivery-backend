import { AuthenticationError, ForbiddenError } from 'apollo-server-express'

const authenticated = next => (root, args, ctx, info) => {
  if (!ctx.user) {
    throw new AuthenticationError('not authenticated')
  }
  return next(root, args, ctx, info)
}

const authorized = (roles, next) => (root, args, ctx, info) => {
  if (!roles.includes(ctx.user.role)) {
    throw new ForbiddenError(`you must be one of the roles ${roles.toString()}`)
  }

  return next(root, args, ctx, info)
}

module.exports = {
  authenticated,
  authorized
}
