const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const CardSchema = new Schema(
  {
    name: String,
    expDate: String,
    cvv: String,
    type: String,
    number: String,
    amount: Number,
    isDeleted: Boolean,
    user: { type: Schema.Types.ObjectId, ref: 'User' }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

CardSchema.plugin(mongoosePaginate)

const Card = mongoose.model('Card', CardSchema)
module.exports = Card
