import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async userAddresses(_, args, ctx) {
      const items = await ctx.models.UserAddress.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async userAddress(_, args, ctx) {
      const item = await ctx.models.UserAddress.findById(args.id).exec()
      if (!item) {
        throw new Error('UserAddress does not exist')
      }
      return item
    },
    async userAddressesByUser(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.UserAddress.find({
        user: userId
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addUserAddress(_, { input }, ctx) {
      const item = new ctx.models.UserAddress(input)
      await item.save()
      return item
    },
    async updateUserAddress(_, { id, input }, ctx) {
      const item = await ctx.models.UserAddress.findOneAndUpdate(
        { _id: id },
        input,
        {
          new: true
        }
      ).exec()
      if (!item) {
        throw new Error('UserAddress not found')
      }
      return item
    },
    async deleteUserAddress(_, { id }, ctx) {
      const result = await ctx.models.UserAddress.deleteOne({ _id: id })

      if (result.deletedCount !== 1) {
        throw new Error('UserAddress not deleted')
      }

      return id
    }
  },
  UserAddress: {
    id(userAddress) {
      return `${userAddress._id}`
    }
  }
}
