import model from './userAddress.model'
import gqlLoader from '../gqlLoader'
import resolvers from './userAddress.resolvers'
import loader from './userAddress.loader'

module.exports = {
  typeDefs: gqlLoader('./userAddress/userAddress.graphql'),
  model,
  resolvers,
  loader
}
