const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose
const passportLocalMongoose = require('passport-local-mongoose')

const UserAddressSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    street: String,
    entrance: String,
    apartment: String,
    intercom: String,
    floor: String,
    comment: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

UserAddressSchema.plugin(mongoosePaginate)

const UserAddress = mongoose.model('UserAddress', UserAddressSchema)
module.exports = UserAddress
