import model from './restaurant.model'
import gqlLoader from '../gqlLoader'
import resolvers from './restaurant.resolvers'
import loader from './restaurant.loader'

module.exports = {
  typeDefs: gqlLoader('./restaurant/restaurant.graphql'),
  model,
  resolvers,
  loader
}
