import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async restaurants(_, args, ctx) {
      const items = await ctx.models.Restaurant.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async restaurant(_, args, ctx) {
      const item = await ctx.models.Restaurant.findById(args.id).exec()
      if (!item) {
        throw new Error('Restaurant does not exist')
      }
      return item
    },
    async restaurantsByCategory(_, { category }, ctx) {
      const items = await ctx.models.Restaurant.find({
        categories: category,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async findRestaurants(_, { text }, ctx) {
      const searchOptions = Object.assign(
        {},
        text && {
          $or: [
            { name: { $regex: new RegExp(`^${text}`, 'i') } },
            { category: { $regex: new RegExp(`^${text}`, 'i') } }
          ]
        }
      )
      const {
        docs: restaurants,
        total,
        limit,
        page,
        pages
      } = await ctx.models.Restaurant.paginate(searchOptions, {
        page: 1,
        limit: 20
      })
      return restaurants
    }
  },
  Mutation: {
    async addRestaurant(_, { input }, ctx) {
      const item = new ctx.models.Restaurant(input)
      await item.save()
      return item
    },
    async updateRestaurant(_, { id, input }, ctx) {
      const item = await ctx.models.Restaurant.findOneAndUpdate(
        { _id: id },
        input,
        {
          new: true
        }
      ).exec()
      if (!item) {
        throw new Error('Restaurant not found')
      }
      return item
    },
    async deleteRestaurant(_, { id }, ctx) {
      const result = await ctx.models.Restaurant.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )

      return id
    }
  },
  Restaurant: {
    id(restaurant) {
      return `${restaurant._id}`
    }
  }
}
