import model from './productComment.model'
import gqlLoader from '../gqlLoader'
import resolvers from './productComment.resolvers'
import loader from './productComment.loader'

module.exports = {
  typeDefs: gqlLoader('./productComment/productComment.graphql'),
  model,
  resolvers,
  loader
}
