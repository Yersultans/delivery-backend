import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async productComments(_, args, ctx) {
      const items = await ctx.models.ProductComment.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async productComment(_, args, ctx) {
      const item = await ctx.models.ProductComment.findById(args.id).exec()
      if (!item) {
        throw new Error('ProductComment does not exist')
      }
      return item
    },
    async productCommentsByProduct(_, { productId }, ctx) {
      const items = await ctx.models.ProductComment.find({
        product: productId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
		async productCommentsByUser(_, { userId }, ctx) {
      const items = await ctx.models.ProductComment.find({
        user: userId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addProductComment(_, { input }, ctx) {
      const item = new ctx.models.ProductComment(input)
      await item.save()
      return item
    },
    async updateProductComment(_, { id, input }, ctx) {
      const item = await ctx.models.ProductComment.findOneAndUpdate(
        { _id: id },
        input
      ).exec()
      if (!item) {
        throw new Error('ProductComment not found')
      }
      return item
    },
    async deleteProductComment(_, { id }, ctx) {
      const result = await ctx.models.ProductComment.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )
      return id
    }
  },
  ProductComment: {
    id(productComment) {
      return `${productComment._id}`
    },
		async user(productComment, _, ctx) {
      const userId = productComment.user
      if (userId) {
        const user = await ctx.loaders.userLoader.load(userId)
        return user
      }
      return null
    },
		async product(productComment, _, ctx) {
      const productId = productComment.product
      if (productId) {
        const product = await ctx.loaders.productLoader.load(productId)
        return product
      }
      return null
    },
  }
}
