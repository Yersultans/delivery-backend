const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const ProductCommentSchema = new Schema(
  {
    text: String,
		rating: {
			type: Number,
			default: 0
		},
		product: { type: Schema.Types.ObjectId, ref: 'Product' },
		user: { type: Schema.Types.ObjectId, ref: 'User' },
    isDeleted: Boolean
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

ProductCommentSchema.plugin(mongoosePaginate)

const ProductComment = mongoose.model('ProductComment', ProductCommentSchema)
module.exports = ProductComment
