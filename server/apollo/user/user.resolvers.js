import { ApolloError } from 'apollo-server-express'
import nodemailer from 'nodemailer'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import path from 'path'
import mongoose from 'mongoose'
import { authenticated, authorized } from '../auth/auth.helper'

const colors = [
  '#52D726',
  '#FFEC00',
  '#FF7300',
  '#FF0000',
  '#007ED6',
  '#7CDDDD',
  '#8464a0',
  '##cea9bc',
  '##323232'
]
export default {
  Query: {
    users: authenticated(async (_, args, ctx) => {
      const users = await ctx.models.User.find({
        isDeleted: { $ne: true }
      }).sort({
        created_at: -1
      })
      return users
    }),
    findUsers: authenticated(async (_, args, ctx) => {
      try {
        const { role } = args.queryUsers || {}
        const { page: queryPage, limit: queryLimit } = args.pagination || {}

        const pageSizeLimit = Number(queryLimit)
        const queryOptions = Object.assign(
          {},
          queryLimit && { limit: pageSizeLimit || 10 },
          queryPage && { page: queryPage || 0 }
        )

        const searchOptions = Object.assign(
          {},
          phoneNumber && {
            phoneNumber: { $regex: new RegExp(`^${phoneNumber}`, 'i') }
          },
          role && { role },
          isDeleted && true
        )
        const {
          docs: findedUsers,
          total,
          limit,
          page,
          pages
        } = await ctx.models.User.paginate(searchOptions, queryOptions)

        const users = role
          ? findedUsers.filter(user => user.role === role)
          : findedUsers
        return { users, total, limit, page, pages }
      } catch (err) {
        console.log('error_: ', err)
        return err
        // res.status(500).send({ err })
      }
    }),
    async user(_, args, ctx) {
      const item = await ctx.models.User.findById(args.id).exec()

      if (!item) {
        throw new Error('User does not exist')
      }
      return item
    },
    usersByRole: authenticated(async (_, { role }, ctx) => {
      const users = await ctx.models.User.find({ role, isDeleted: false })
      return users
    }),
    async statistics(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      let items = []
      const orders = await ctx.models.Order.find({ buyer: userId })
      const total = orders.reduce(
        (partialSum, a) => partialSum + a.totalPrice,
        0
      )
      orders.forEach(order => {
        items = items.concat(order.items)
      })
      const groups = orders.reduce((groups, item) => {
        const dateString = item?.created_at
          ?.toISOString()
          .split('T')[0]
          .split('-')
        const date = `${dateString[0]}-${dateString[1]}`
        if (!groups[date]) {
          groups[date] = []
        }
        groups[date].push(item)
        return groups
      }, {})
      const groupArrays = Object.keys(groups).map(date => {
        const total = groups[date].reduce(
          (partialSum, a) => partialSum + a.totalPrice,
          0
        )
        return {
          date: new Date(date),
          price: total
        }
      })
      const groupsCategory = items.reduce((groups, item) => {
        const category = item?.category
        if (!groups[category]) {
          groups[category] = []
        }
        groups[category].push(item)
        return groups
      }, {})

      const groupCategoryArrays = Object.keys(groupsCategory).map(
        (category, index) => {
          const total = groupsCategory[category].reduce(
            (partialSum, a) => partialSum + a.totalPrice,
            0
          )
          return {
            name: category,
            price: total,
            color: colors[index],
            legendFontColor: '#7F7F7F',
            legendFontSize: 15
          }
        }
      )
      return {
        total: total,
        spendAllMonths: groupArrays,
        paymentByCategories: groupCategoryArrays
      }
    },
    usersByShop: authenticated(async (_, { shopId }, ctx) => {
      const users = await ctx.models.User.find({
        shop: shopId,
        isDeleted: false
      })
      return users
    })
  },
  Mutation: {
    async addUser(_, { input }, ctx) {
      try {
        const { User } = ctx.models
        const { password } = input
        delete input.password
        const user = await User.register(new User(input), `${password}`)

        await user.save()

        return user
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate username or email
            throw new ApolloError('User already exists')
          }
          throw new ApolloError(err)
        }
      }
    },
    updateUser: authenticated(async (_, { id, input }, ctx) => {
      const userId = id || ctx.user._id
      const { password } = input
      delete input.password
      const user = await ctx.models.User.findOneAndUpdate(
        { _id: userId },
        input,
        {
          new: true
        }
      ).exec()
      if (password) {
        const sanitizedUser = await user.setPassword(password)
        await sanitizedUser.save()
      }
      return user
    }),
    deleteUser: authenticated(
      authorized(['admin'], async (_, { id }, ctx) => {
        try {
          const result = await ctx.models.User.deleteOne({ _id: id })

          if (result.deletedCount !== 1) {
            throw new Error('User not deleted')
          }
          return id
        } catch (err) {
          throw new ApolloError(err)
        }
      })
    ),
    editUser: authenticated(async (_, { id, input }, ctx) => {
      try {
        const { password } = input
        delete input.password
        const user = await ctx.models.User.findOneAndUpdate(
          { _id: id },
          input,
          {
            new: true
          }
        ).exec()
        if (!user) {
          throw new Error('User not found')
        }
        if (password) {
          const sanitizedUser = await user.setPassword(password)
          await sanitizedUser.save()
        }
        return user
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate phoneNumber
            throw new ApolloError('User not updated')
          }

          // Some other error
          throw new ApolloError(err)
        }
      }
    })
  },
  User: {
    id(user) {
      return `${user._id}`
    },
    async shop(user, _, ctx) {
      const shopId = user.shop
      if (shopId) {
        const shop = await ctx.loaders.shopLoader.load(shopId)
        return shop
      }
      return null
    }
    // async cards(user, _, ctx) {
    //   if (!user.cards) return []
    //   const cards = await ctx.loaders.cardLoader.loadMany(
    //     user.cards.filter(program => program != null)
    //   )
    //   return cards
    // },
    // async defaultCard(user, _, ctx) {
    //   const defaultCardId = user.defaultCard
    //   if (defaultCardId) {
    //     const defaultCard = await ctx.loaders.cardLoader.load(defaultCardId)
    //     return defaultCard
    //   }
    //   return null
    // }
  }
}
