const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const ShopProductSchema = new Schema(
  {
    shop: { type: Schema.Types.ObjectId, ref: 'Shop' },
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    price: Number,
    oldPrice: Number,
    isDiscount: {
      type: Boolean,
      default: false
    },
    isActive: {
      type: Boolean,
      default: false
    },
    isDeleted: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

ShopProductSchema.plugin(mongoosePaginate)

const ShopProduct = mongoose.model('ShopProduct', ShopProductSchema)
module.exports = ShopProduct
