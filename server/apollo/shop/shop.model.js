const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const ShopSchema = new Schema(
  {
    name: String,
    imageUrl: String,
    phoneNumber: String,
    rating: Number,
    priceRating: Number,
    address: {
      street: String,
      entrance: String,
      apartment: String,
      intercom: String,
      floor: String,
      comment: String
    },
    categories: [String],
    isActive: {
      type: Boolean,
      default: false
    },
    isDeleted: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

ShopSchema.plugin(mongoosePaginate)

const Shop = mongoose.model('Shop', ShopSchema)
module.exports = Shop
