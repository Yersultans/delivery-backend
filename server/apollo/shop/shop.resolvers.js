import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async shops(_, args, ctx) {
      const items = await ctx.models.Shop.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async shop(_, args, ctx) {
      const item = await ctx.models.Shop.findById(args.id).exec()
      if (!item) {
        throw new Error('Shop does not exist')
      }
      return item
    },
    async shopsByCategory(_, { category }, ctx) {
      const items = await ctx.models.Shop.find({
        categories: category,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async findShops(_, { text }, ctx) {
      const searchOptions = Object.assign(
        {},
        text && {
          $or: [
            { name: { $regex: new RegExp(`^${text}`, 'i') } },
            { category: { $regex: new RegExp(`^${text}`, 'i') } }
          ]
        }
      )
      const {
        docs: shops,
        total,
        limit,
        page,
        pages
      } = await ctx.models.Shop.paginate(searchOptions, { page: 1, limit: 20 })
      return shops
    },
    async shopProductsByShop(_, { shopId }, ctx) {
      const shopProducts = await ctx.models.ShopProduct.find({
        shop: shopId
      }).sort({
        _id: -1
      })
      return shopProducts
    },
    async discountShopProducts(_, args, ctx) {
      const shopProducts = await ctx.models.ShopProduct.find({
        isDiscount: true
      }).sort({
        _id: -1
      })
      return shopProducts
    },
    async shopProductsByProduct(_, { productId }, ctx) {
      const shopProducts = await ctx.models.ShopProduct.find({
        product: productId
      }).sort({
        _id: -1
      })
      const interestsProduct = await ctx.user.interests
      if (interestsProduct.indexOf(productId) !== -1) {
        interestsProduct.splice(interestsProduct.indexOf(productId), 1)
      }
      interestsProduct.unshift(productId)
      await ctx.models.User.findOneAndUpdate(
        { _id: ctx.user._id },
        { interests: interestsProduct }
      ).exec()
      return shopProducts
    },
    async interestProducts(_, args, ctx) {
      const interestProducts = await ctx.models.ShopProduct.find({
        product: ctx.user.interests
      }).sort({
        price: 1
      })

      const lowPrices = ctx.user.interests.map(
        item =>
          interestProducts.filter(
            ind => ind.product.toString() == item.toString()
          )[0]
      )

      return lowPrices
    }
  },
  Mutation: {
    async addShop(_, { input }, ctx) {
      const item = new ctx.models.Shop(input)
      await item.save()
      return item
    },
    async updateShop(_, { id, input }, ctx) {
      const item = await ctx.models.Shop.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('Shop not found')
      }
      return item
    },
    async deleteShop(_, { id }, ctx) {
      const result = await ctx.models.Shop.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )

      return id
    },
    async addShopProduct(_, { input }, ctx) {
      const item = new ctx.models.ShopProduct(input)
      await item.save()
      return item
    },
    async updateShopProduct(_, { id, input }, ctx) {
      const item = await ctx.models.ShopProduct.findOneAndUpdate(
        { _id: id },
        input,
        {
          new: true
        }
      ).exec()
      if (!item) {
        throw new Error('ShopProduct not found')
      }
      return item
    },
    async deleteShopProduct(_, { id }, ctx) {
      const result = await ctx.models.ShopProduct.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )

      return id
    }
  },
  Shop: {
    id(shop) {
      return `${shop._id}`
    }
  },
  ShopProduct: {
    id(shopProduct) {
      return `${shopProduct._id}`
    },
    async shop(shopProduct, _, ctx) {
      const shopId = shopProduct.shop
      if (shopId) {
        const shop = await ctx.loaders.shopLoader.load(shopId)
        return shop
      }
      return null
    },
    async product(shopProduct, _, ctx) {
      const productId = shopProduct.product
      if (productId) {
        const product = await ctx.loaders.productLoader.load(productId)
        return product
      }
      return null
    }
  }
}
