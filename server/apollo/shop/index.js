import model from './shop.model'
import shopProductModel from './shopProduct.model'
import gqlLoader from '../gqlLoader'
import resolvers from './shop.resolvers'
import loader from './shop.loader'

module.exports = {
  typeDefs: gqlLoader('./shop/shop.graphql'),
  shopProductModel,
  model,
  resolvers,
  loader
}
