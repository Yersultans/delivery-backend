import model from './category.model'
import gqlLoader from '../gqlLoader'
import resolvers from './category.resolvers'
import loader from './category.loader'

module.exports = {
  typeDefs: gqlLoader('./category/category.graphql'),
  model,
  resolvers,
  loader
}
