const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const CategorySchema = new Schema(
  {
    name: String,
    identifier: String,
    isActive: Boolean,
    isDeleted: Boolean
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

CategorySchema.plugin(mongoosePaginate)

const Category = mongoose.model('Category', CategorySchema)
module.exports = Category
