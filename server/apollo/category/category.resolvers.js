import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async categories(_, args, ctx) {
      const items = await ctx.models.Category.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async category(_, args, ctx) {
      const item = await ctx.models.Category.findById(args.id).exec()
      if (!item) {
        throw new Error('Category does not exist')
      }
      return item
    },
    async activeCategories(_, args, ctx) {
      const items = await ctx.models.Category.find({
        isActive: true,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addCategory(_, { input }, ctx) {
      const item = new ctx.models.Category(input)
      await item.save()
      return item
    },
    async updateCategory(_, { id, input }, ctx) {
      const item = await ctx.models.Category.findOneAndUpdate(
        { _id: id },
        input,
        {
          new: true
        }
      ).exec()
      if (!item) {
        throw new Error('Category not found')
      }
      return item
    },
    async deleteCategory(_, { id }, ctx) {
      const result = await ctx.models.Category.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )

      return id
    }
  },
  Category: {
    id(Category) {
      return `${Category._id}`
    }
  }
}
