import { authenticated, authorized } from '../auth/auth.helper'

const hasCategory = ({ categories, productCategory }) => {
  let isCategory = false
  categories.forEach(category => {
    if (category === productCategory) {
      isCategory = true
    }
  })
  return isCategory
}

module.exports = {
  Query: {
    async products(_, args, ctx) {
      const items = await ctx.models.Product.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async product(_, args, ctx) {
      const item = await ctx.models.Product.findById(args.id).exec()
      if (!item) {
        throw new Error('Product does not exist')
      }
      return item
    },
    async productsByCategory(_, { category }, ctx) {
      const items = await ctx.models.Product.find({
        category: category,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async findProducts(_, { text }, ctx) {
      const searchOptions = Object.assign(
        {},
        text && {
          $or: [
            { name: { $regex: new RegExp(`^${text}`, 'i') } },
            { category: { $regex: new RegExp(`^${text}`, 'i') } }
          ]
        }
      )
      const {
        docs: products,
        total,
        limit,
        page,
        pages
      } = await ctx.models.Product.paginate(searchOptions, {
        page: 1,
        limit: 20
      })
      return products
    }
  },
  Mutation: {
    async addProduct(_, { input }, ctx) {
      const item = new ctx.models.Product(input)
      await item.save()
      return item
    },
    async updateProduct(_, { id, input }, ctx) {
      const item = await ctx.models.Product.findOneAndUpdate(
        { _id: id },
        input
      ).exec()
      if (!item) {
        throw new Error('Product not found')
      }
      return item
    },
    async deleteProduct(_, { id }, ctx) {
      const result = await ctx.models.Product.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )
      return id
    }
  },
  Product: {
    id(product) {
      return `${product._id}`
    }
  }
}
