const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const ProductSchema = new Schema(
  {
    name: String,
    description: String,
    category: String,
    imageUrl: String,
    proteins: Number,
    fats: Number,
    carbohydrates: Number,
    calories: Number,
    isVegan: Boolean,
    isDeleted: Boolean
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

ProductSchema.plugin(mongoosePaginate)

const Product = mongoose.model('Product', ProductSchema)
module.exports = Product
