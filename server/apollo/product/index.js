import model from './product.model'
import gqlLoader from '../gqlLoader'
import resolvers from './product.resolvers'
import loader from './product.loader'

module.exports = {
  typeDefs: gqlLoader('./product/product.graphql'),
  model,
  resolvers,
  loader
}
