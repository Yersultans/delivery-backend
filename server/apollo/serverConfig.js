import merge from 'lodash/merge'
import { PubSub } from 'apollo-server-express'
import { GraphQLScalarType } from 'graphql'
import passport from 'passport'
import auth from './auth'
import user from './user'
import restaurant from './restaurant'
import food from './food'
import card from './card'
import userAddress from './userAddress'
import order from './order'
import category from './category'
import productComment from './productComment'

import product from './product'
import shop from './shop'

import gqlLoader from './gqlLoader'

require('./../passportHelper')(passport)

const pubsub = new PubSub()

const resolverMap = {
  DateTime: new GraphQLScalarType({
    name: 'DateTime',
    description: 'A date and time, represented as an ISO-8601 string',
    serialize: value => value.toISOString(),
    parseValue: value => new Date(value),
    parseLiteral: ast => new Date(ast.value)
  })
}

const serverConfig = {
  introspection: true,
  uploads: false,
  playground: true,
  tracing: true,
  typeDefs: [
    gqlLoader('./index.graphql'),
    user.typeDefs,
    auth.typeDefs,
    restaurant.typeDefs,
    food.typeDefs,
    card.typeDefs,
    userAddress.typeDefs,
    order.typeDefs,
    category.typeDefs,
    product.typeDefs,
    shop.typeDefs,
    productComment.typeDefs
    // payment.typeDefs,
    // notification.typeDefs
  ].join(' '),
  resolvers: merge(
    {},
    auth.resolvers,
    user.resolvers,
    restaurant.resolvers,
    food.resolvers,
    card.resolvers,
    userAddress.resolvers,
    order.resolvers,
    category.resolvers,
    product.resolvers,
    shop.resolvers,
    productComment.resolvers,
    resolverMap
  ),
  context: ({ req, connection }) => {
    return {
      user: connection ? null : req.user,
      logout: connection ? function emptyFunction() {} : req.logout,
      loaders: {
        userLoader: user.loader,
        restaurantLoader: restaurant.loader,
        foodLoader: food.loader,
        cardLoader: card.loader,
        userAddressLoader: userAddress.loader,
        orderLoader: order.loader,
        categoryLoader: category.loader,
        productLoader: product.loader,
        shopLoader: shop.loader,
        productCommentLoader: productComment.loader
      },
      models: {
        User: user.model,
        Restaurant: restaurant.model,
        Food: food.model,
        Card: card.model,
        UserAddress: userAddress.model,
        Order: order.model,
        Category: category.model,
        Product: product.model,
        Shop: shop.model,
        ShopProduct: shop.shopProductModel,
        ProductComment: productComment.model
      },
      pubsub
    }
  }
}
module.exports = serverConfig
