const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const OrderSchema = new Schema(
  {
    items: [
      {
        product: { type: Schema.Types.ObjectId, ref: 'Product' },
        shop: { type: Schema.Types.ObjectId, ref: 'Shop' },
        name: String,
        description: String,
        price: Number,
        category: String,
        imageUrl: String,
        proteins: Number,
        fats: Number,
        carbohydrates: Number,
        calories: Number,
        isVegan: Boolean,
        oldPrice: Number,
        isDiscount: Boolean,
        quantity: Number,
        totalPrice: Number
      }
    ],
    shops: [{ type: Schema.Types.ObjectId, ref: 'Shop' }],
    orderProgresses: [
      {
        type: {
          type: String,
          enum: ['shop', 'findCourier', 'delivery']
        },
        shop: { type: Schema.Types.ObjectId, ref: 'Shop' },
        isCompleted: {
          type: Boolean,
          default: false
        }
      }
    ],
    status: String,
    isDeleted: Boolean,
    buyer: { type: Schema.Types.ObjectId, ref: 'User' },
    courier: { type: Schema.Types.ObjectId, ref: 'User' },
    address: {
      street: String,
      entrance: String,
      apartment: String,
      intercom: String,
      floor: String,
      comment: String
    },
    deliveryPrice: Number,
    priceWithoutDelivery: Number,
    totalPrice: Number,
    paymentMethod: {
      type: String,
      enum: ['card', 'cash']
    },
    card: { type: Schema.Types.ObjectId, ref: 'Card' },
    identifier: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

OrderSchema.plugin(mongoosePaginate)

const Order = mongoose.model('Order', OrderSchema)
module.exports = Order
