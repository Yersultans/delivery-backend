import model from './order.model'
import gqlLoader from '../gqlLoader'
import resolvers from './order.resolvers'
import loader from './order.loader'

module.exports = {
  typeDefs: gqlLoader('./order/order.graphql'),
  model,
  resolvers,
  loader
}
