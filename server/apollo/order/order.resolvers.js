import { authenticated, authorized } from '../auth/auth.helper'

const getIdentifier = ({ id }) => {
  const date = new Date()
  let identifier = `${date.getFullYear() -
    2000}${date.getMonth()}${date.getDate()}-${id.substr(-3)}`
  return identifier
}

module.exports = {
  Query: {
    async orders(_, args, ctx) {
      const items = await ctx.models.Order.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async ordersByShop(_, { shopId }, ctx) {
      const items = await ctx.models.Order.find({
        shops: shopId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async userOrders(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.models.user._id
      const items = await ctx.models.Order.find({
        buyer: userId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async order(_, args, ctx) {
      const item = await ctx.models.Order.findById(args.id).exec()
      if (!item) {
        throw new Error('Order does not exist')
      }
      return item
    },
    async courierActiveOrders(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.Order.find({
        courier: userId,
        status: 'Completed',
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async ordersWithOutCourier(_, args, ctx) {
      const items = await ctx.models.Order.find({
        courier: null,
        status: 'findCourier',
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addOrder(_, { input }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      console.log(orderId)
      if (item.status !== 'inProgress') {
        item.status = 'inProgress'
        item.courier = userId
        if (item.orderProgresses[0]) {
          item.orderProgresses[0].isCompleted = true
        }
      } else {
        item.status = 'Completed'
        item.courier = userId
        if (item.orderProgresses[1]) {
          item.orderProgresses[1].isCompleted = true
        }
        if (item.orderProgresses[2]) {
          item.orderProgresses[2].isCompleted = true
        }
        if (item.orderProgresses[3]) {
          item.orderProgresses[3].isCompleted = true
        }
        if (item.orderProgresses[4]) {
          item.orderProgresses[4].isCompleted = true
        }
      }
      console.log(item.orderProgress)
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async updateOrder(_, { id, input }, ctx) {
      const item = await ctx.models.Order.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('Order not found')
      }
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async deleteOrder(_, { id }, ctx) {
      const result = await ctx.models.Order.deleteOne({ _id: id })

      if (result.deletedCount !== 1) {
        throw new Error('Order not deleted')
      }

      ctx.pubsub.publish('booking', {
        booking: {
          mutation: 'Deleted',
          data: item
        }
      })

      return id
    },
    async addCourierToOrder(_, { orderId, userId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'inProgress'
      item.courier = userId
      if (item.orderProgresses[0]) {
        item.orderProgresses[0].isCompleted = true
      }
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async pickUpProductFromShop(_, { orderId, shopId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      console.log('item.orderProgresses ', item.orderProgresses)
      item.orderProgresses = item.orderProgresses.map(orderProgress => {
        if (
          orderProgress?.type === 'shop' &&
          orderProgress?.shop.toString() === shopId.toString()
        ) {
          orderProgress.isCompleted = true
          return orderProgress
        }
        return orderProgress
      })
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async finishOrder(_, { orderId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'finished'
      item.orderProgresses = item.orderProgresses.map(orderProgress => {
        if (orderProgress.type === 'delivery') {
          orderProgress.isCompleted = true
        }
      })
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    }
  },
  Order: {
    id(order) {
      return `${order._id}`
    },
    async shops(order, _, ctx) {
      if (!order.shops) return []
      const shops = await ctx.loaders.shopLoader.loadMany(
        order.shops.filter(shop => shop != null)
      )
      return shops
    },
    async buyer(order, _, ctx) {
      const userId = order.buyer
      if (userId) {
        const user = await ctx.loaders.userLoader.load(userId)
        return user
      }
      return null
    },
    async courier(order, _, ctx) {
      const userId = order.courier
      if (userId) {
        const user = await ctx.loaders.userLoader.load(userId)
        return user
      }
      return null
    }
  },
  Item: {
    async product(item, _, ctx) {
      const productId = item.product
      if (productId) {
        const product = await ctx.loaders.productLoader.load(productId)
        return product
      }
      return null
    },
    async shop(item, _, ctx) {
      const shopId = item.shop
      if (shopId) {
        const shop = await ctx.loaders.shopLoader.load(shopId)
        return shop
      }
      return null
    }
  },
  OrderProgress: {
    async shop(orderProgress, _, ctx) {
      const shopId = orderProgress.shop
      if (shopId) {
        const shop = await ctx.loaders.shopLoader.load(shopId)
        return shop
      }
      return null
    }
  }
}
