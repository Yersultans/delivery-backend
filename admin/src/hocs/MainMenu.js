import React from 'react'
import PropTypes from 'prop-types'
import { Link, useHistory } from 'react-router-dom'
import {
  LogoutOutlined,
  TeamOutlined,
  RestOutlined,
  RadarChartOutlined,
  ShopOutlined,
  ShoppingOutlined,
  UnorderedListOutlined,
  GatewayOutlined
} from '@ant-design/icons'
import { Menu } from 'antd'
import { useAuth } from '../context/useAuth'

import Loading from '../pages/shared/Loading'

function MainMenu({ currentUrl }) {
  const { user, logout } = useAuth()
  const history = useHistory()

  const adminMenu = [
    { link: '/users', name: 'Users', icon: TeamOutlined },
    // { link: '/restaurants', name: 'Заведения', icon: RestOutlined },
    { link: '/categories', name: 'Categories', icon: RadarChartOutlined },
    { link: '/shops', name: 'Shops', icon: ShopOutlined },
    { link: '/products', name: 'Products', icon: ShoppingOutlined }
  ]

  const shopStaffMenu = [
    { link: '/generalShop', name: 'General', icon: ShopOutlined },
    { link: '/shopProducts', name: 'Products', icon: GatewayOutlined },
    { link: '/ordersShop', name: 'Orders', icon: UnorderedListOutlined },
    { link: '/usersShop', name: 'Staff', icon: TeamOutlined }
  ]

  const userToMenu = {
    admin: adminMenu,
    shopStaff: shopStaffMenu
  }

  if (!user) {
    return <Loading />
  }

  const currentMenu = userToMenu[user.role] || []

  const onLogoutClick = () => {
    logout()
    history.push('/login')
  }

  if (user) {
    return (
      <Menu selectedKeys={[currentUrl]} mode="inline" theme="light">
        {currentMenu.map(item => (
          <Menu.Item key={item.link} icon={<item.icon />}>
            <Link to={item.link} key={item.link}>
              {item.name}
            </Link>
          </Menu.Item>
        ))}

        <Menu.Item
          key="logout"
          icon={<LogoutOutlined />}
          onClick={onLogoutClick}
          danger
        >
          Logout
        </Menu.Item>
      </Menu>
    )
  }
  return <React.Fragment />
}

MainMenu.propTypes = {
  currentUrl: PropTypes.string.isRequired
}

export default MainMenu
