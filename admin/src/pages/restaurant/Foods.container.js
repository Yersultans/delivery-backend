import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import Foods from './Foods.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_FOODS = gql`
  query foodsByRestaurant($restaurantId: ID) {
    foodsByRestaurant(restaurantId: $restaurantId) {
      id
      name
      description
      imageUrl
      price
      category
      proteins
      fats
      carbohydrates
      calories
      isVegan
      isActive
    }
    categories {
      id
      name
      identifier
    }
  }
`

const ADD_FOOD = gql`
  mutation addFood($input: FoodInput) {
    addFood(input: $input) {
      id
      name
      description
      imageUrl
      price
      category
      proteins
      fats
      carbohydrates
      calories
      isVegan
      isActive
    }
  }
`

const DELETE_FOOD = gql`
  mutation deleteFood($id: ID!) {
    deleteFood(id: $id)
  }
`
const UPDATE_FOOD = gql`
  mutation updateFood($id: ID!, $input: FoodInput) {
    updateFood(id: $id, input: $input) {
      id
      name
      description
      imageUrl
      price
      category
      proteins
      fats
      carbohydrates
      calories
      isVegan
      isActive
    }
  }
`

const FoodsContainer = ({ restaurantId }) => {
  const { showLoading, hideLoading } = useLoading()
  const [allFoods, setAllFoods] = React.useState([])
  const [dataCategoryes, setDataCategories] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_FOODS, {
    variables: {
      restaurantId: restaurantId
    }
  })
  const [updateFood] = useMutation(UPDATE_FOOD)
  const [addFood, { error: errorAddFood }] = useMutation(ADD_FOOD, {
    update(cache, { data: { addFood: addFoodItem } }) {
      const { foodsByRestaurant } = cache.readQuery({
        query: GET_FOODS,
        variables: {
          restaurantId: restaurantId
        }
      })
      cache.writeQuery({
        query: GET_FOODS,
        variables: {
          restaurantId: restaurantId
        },
        data: { foodsByRestaurant: foodsByRestaurant.concat([addFoodItem]) }
      })
      hideLoading()
      toast.success('Пользователь добавлен')
    }
  })

  const [deleteFood] = useMutation(DELETE_FOOD, {
    update(cache, { data: { deleteFood: id } }) {
      const { foodsByRestaurant } = cache.readQuery({
        query: GET_FOODS,
        variables: {
          restaurantId: restaurantId
        }
      })
      cache.writeQuery({
        query: GET_FOODS,
        variables: {
          restaurantId: restaurantId
        },
        data: {
          foodsByRestaurant: foodsByRestaurant.filter(
            restaurant => restaurant.id !== id
          )
        }
      })
      hideLoading()
      toast.success('Блюдо успешно удалено')
    }
  })

  React.useEffect(() => {
    if (errorAddFood) {
      console.log(errorAddFood)
    }
  }, [errorAddFood])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllFoods(data.foodsByRestaurant)
      setDataCategories(data.categories)
    }
  }, [data, loading, error])

  const addFoodHandler = value => {
    addFood({
      variables: {
        input: { ...value, restaurant: restaurantId }
      }
    })
  }

  const updateFoodHandler = (id, values) => {
    toast.success('Заведение успешно отредактировано')
    updateFood({ variables: { id, input: values } })
  }

  const deleteFoodHandler = id => {
    showLoading()
    deleteFood({ variables: { id: id } })
    hideLoading()
    toast.success('Заведение успешно удалено')
  }

  if (loading) {
    return <Loading />
  }

  return (
    <Foods
      foods={allFoods}
      categories={dataCategoryes}
      addFoodHandler={addFoodHandler}
      updateFoodHandler={updateFoodHandler}
      deleteFoodHandler={deleteFoodHandler}
    />
  )
}

export default FoodsContainer
