import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import {
  PlusOutlined,
  SearchOutlined,
  CloseOutlined,
  CheckOutlined
} from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import EditFood from './EditFood'
import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`
const Foods = ({
  foods,
  categories,
  addFoodHandler,
  updateFoodHandler,
  deleteFoodHandler
}) => {
  const [form] = Form.useForm()
  const [formForEdit] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [imageUrl, setImageUrl] = React.useState(null)
  const [display, setDisplay] = React.useState('default')
  const [editFood, setEditFood] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listFoods, setListFoods] = React.useState([])

  React.useEffect(() => {
    if (foods.length > 0) {
      const descriptiveFoods = foods.map(restaurant => {
        return {
          ...restaurant
        }
      })
      let searchArray = descriptiveFoods

      if (searchName) {
        searchArray = searchArray.filter(restaurant => {
          return restaurant.name?.toLowerCase().includes(searchName)
        })
      }
      setListFoods(searchArray)
    }
  }, [foods, searchName])

  const columns = [
    {
      title: 'Фото',
      dataIndex: 'imageUrl',
      width: '15%',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Icon" />
        ) : (
          <FeilDiv>Нет Фото</FeilDiv>
        )
      }
    },
    {
      title: 'Название',
      dataIndex: 'name',
      width: '25%'
    },
    {
      title: 'Цена',
      dataIndex: 'price',
      width: '15%'
    },
    {
      title: 'Активен',
      dataIndex: 'isActive',
      width: '15%',
      render: item => {
        return item ? (
          <SuccessDiv>Активен</SuccessDiv>
        ) : (
          <FeilDiv>Неактивен</FeilDiv>
        )
      }
    },
    {
      title: 'Действие',
      width: '30%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setDisplay('edit')
              setEditFood(
                foods.find(food => food.id.toString() === item.id.toString())
              )
              setImageUrl(item.imageUrl)
              console.log('editFood', editFood)
            }}
          >
            Редактировать
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const handleCreate = values => {
    addFoodHandler({
      ...values,
      isActive: values.isActive ? true : false,
      proteins: parseInt(values.proteins),
      fats: parseInt(values.fats),
      carbohydrates: parseInt(values.carbohydrates),
      calories: parseInt(values.calories),
      price: parseInt(values.price)
    })
    setModalVisible(false)
    setImageUrl(null)
  }

  const handleUpdate = (id, values) => {
    updateFoodHandler(id, {
      ...values,
      isActive:
        typeof values.isActive !== 'undefined'
          ? values.isActive
          : editFood.isActive,
      proteins: parseInt(values.proteins),
      fats: parseInt(values.fats),
      carbohydrates: parseInt(values.carbohydrates),
      calories: parseInt(values.calories),
      price: parseInt(values.price)
    })
    setImageUrl(null)
    setDisplay('default')
    setEditFood(null)
  }

  const handleDelete = id => {
    console.log('id', id)
    setImageUrl(null)
    setDisplay('default')
    setEditFood(null)
    deleteFoodHandler(id)
  }

  return (
    <>
      <Table
        dataSource={listFoods}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Название"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> Новое Блюдо
            </Button>
          </StyledHeaderContainer>
        )}
      />

      <Modal
        visible={modalVisible}
        title="Новое Блюдо"
        okText="Создать"
        cancelText="Отмена"
        key="create"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.imageUrl

              handleCreate({ ...values, imageUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Название Блюда', 'Название Блюда')}
            name="name"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Название Блюда`
              }
            ]}
          >
            <Input placeholder="Название" />
          </FormItem>
          <FormItem
            key="description"
            label={getTooltip('Описание Блюда', 'Описание Блюда')}
            name="description"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Описание Блюда`
              }
            ]}
          >
            <Input placeholder="Описание" />
          </FormItem>
          <FormItem
            key="price"
            label={getTooltip('Цена Блюда', 'Цена Блюда')}
            name="price"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Цена Блюда`
              }
            ]}
          >
            <Input type="number" placeholder="Цена" />
          </FormItem>
          <FormItem
            key="category"
            label={getTooltip('Категория ', 'Категория Блюда')}
            name="category"
            rules={[
              {
                required: true,
                message: `Пожалуйста, выберите Категория Блюда`
              }
            ]}
          >
            <Select mode="single" placeholder="выберите Категорию" showSearch>
              {categories &&
                categories.map(category => (
                  <Select.Option
                    key={category.identifier}
                    value={category.identifier}
                  >
                    {category.name}
                  </Select.Option>
                ))}
            </Select>
          </FormItem>
          <FormItem
            key="proteins"
            label={getTooltip('Белки в Блюде', 'Белки в Блюде')}
            name="proteins"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Белки в Блюде`
              }
            ]}
          >
            <Input type="number" placeholder="Белки в Блюде" />
          </FormItem>
          <FormItem
            key="fats"
            label={getTooltip('Жиры в Блюде', 'Жиры в Блюде')}
            name="fats"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Жиры в Блюде`
              }
            ]}
          >
            <Input type="number" placeholder="Жиры в Блюде" />
          </FormItem>
          <FormItem
            key="carbohydrates"
            label={getTooltip('Углеводы в Блюде', 'Углеводы в Блюде')}
            name="carbohydrates"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Углеводы в Блюде`
              }
            ]}
          >
            <Input type="number" placeholder="Углеводы в Блюде" />
          </FormItem>
          <FormItem
            key="calories"
            label={getTooltip('Калории в Блюде', 'Калории в Блюде')}
            name="calories"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Калории в Блюде`
              }
            ]}
          >
            <Input type="number" placeholder="Калории в Блюде" />
          </FormItem>
          <FormItem
            key="isActive"
            label={getTooltip('Показывать?', 'Показывать ли блюдо в аппе ?')}
            name="isActive"
            valuePropName="isActive"
          >
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </FormItem>
          <FormItem
            key="imageUrl"
            label={getTooltip('Фото', 'Фото Блюда')}
            name="imageUrl"
            rules={[
              {
                required: false,
                message: `Пожалуйста, загрузите Фото`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setImageUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={imageUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
      {editFood && (
        <EditFood
          visible={display === 'edit'}
          onCancel={() => {
            setImageUrl(null)
            setDisplay('default')
            setEditFood(null)
          }}
          categories={categories}
          title="Редактировать Блюдо"
          editFood={editFood}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
        />
      )}
    </>
  )
}

Foods.propTypes = {
  foods: PropTypes.arrayOf(PropTypes.object).isRequired,
  addFoodHandler: PropTypes.func.isRequired,
  updateFoodHandler: PropTypes.func.isRequired
}

export default Foods
