import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditRestaurant = ({
  visible,
  onCancel,
  title,
  editRestaurant,
  onUpdate,
  onDelete,
  imageUrl,
  setImageUrl
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Создать"
      cancelText="Закрыть"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editRestaurant.id)
            })
          }}
        >
          Удалить
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Отмена
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate(editRestaurant.id, values)
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Сохранить
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Название заведения', 'Название заведения')}
          name="name"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Название заведения`
            }
          ]}
          initialValue={editRestaurant.name}
        >
          <Input placeholder="Название" />
        </FormItem>
        <FormItem
          key="phoneNumber"
          label={getTooltip('Телефон заведения', 'Телефон заведения')}
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Телефон заведения`
            }
          ]}
          initialValue={editRestaurant.phoneNumber}
        >
          <Input placeholder="Телефон" />
        </FormItem>
        <Form.Item
          label="Адрес"
          name="address"
          initialValue={{
            street: editRestaurant.address.street,
            entrance: editRestaurant.address.entrance,
            apartment: editRestaurant.address.apartment,
            intercom: editRestaurant.address.intercom,
            floor: editRestaurant.address.floor
          }}
        >
          <Input.Group compact>
            <Form.Item name={['address', 'street']} noStyle>
              <Input style={{ width: '100%' }} placeholder="Улица" />
            </Form.Item>
            <Form.Item name={['address', 'entrance']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Подъезд" />
            </Form.Item>
            <Form.Item name={['address', 'apartment']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Квартира" />
            </Form.Item>
            <Form.Item name={['address', 'intercom']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Домафон" />
            </Form.Item>
            <Form.Item name={['address', 'floor']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Этаж" />
            </Form.Item>
          </Input.Group>
        </Form.Item>
        <FormItem
          key="rating"
          label={getTooltip('Рейтинг заведения', 'Рейтинг заведения')}
          name="rating"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Рейтинг заведения`
            }
          ]}
          initialValue={editRestaurant.rating}
        >
          <Input type="number" placeholder="Рейтинг" />
        </FormItem>
        <FormItem
          key="priceRating"
          label={getTooltip(
            'Ценавой Рейтинг заведения',
            'Ценавой Рейтинг заведения'
          )}
          name="priceRating"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Ценавой Рейтинг заведения`
            }
          ]}
          initialValue={editRestaurant.priceRating}
        >
          <Input type="number" placeholder="Ценавой Рейтинг" />
        </FormItem>
        <FormItem
          key="imageUrl"
          label={getTooltip('Аватарка', 'Аватарка пользователя')}
          name="imageUrl"
          rules={[
            {
              required: false,
              message: `Пожалуйста, загрузите Аватарку`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setImageUrl(value)
              }}
            />
            <StyledAvatar
              size="96"
              shape="square"
              src={imageUrl || editRestaurant.imageUrl}
            />
          </ImageUploadContainer>
        </FormItem>
        <FormItem
          key="isActive"
          label={getTooltip('Показывать?', 'Показывать ли завидение в аппе ?')}
          name="isActive"
          valuePropName="isActive"
        >
          <Switch
            defaultChecked={editRestaurant?.isActive}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
      </Form>
    </Modal>
  )
}

EditRestaurant.propTypes = {
  editFood: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditRestaurant
