import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditFood = ({
  visible,
  onCancel,
  title,
  editFood,
  onUpdate,
  onDelete,
  imageUrl,
  setImageUrl,
  categories
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Создать"
      cancelText="Закрыть"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editFood.id)
            })
          }}
        >
          Удалить
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Отмена
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                delete values.imageUrl
                onUpdate(editFood.id, { ...values, imageUrl })
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Сохранить
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Название Блюда', 'Название Блюда')}
          name="name"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Название Блюда`
            }
          ]}
          initialValue={editFood.name}
        >
          <Input placeholder="Название" />
        </FormItem>
        <FormItem
          key="description"
          label={getTooltip('Описание Блюда', 'Описание Блюда')}
          name="description"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Описание Блюда`
            }
          ]}
          initialValue={editFood.description}
        >
          <Input placeholder="Описание" />
        </FormItem>
        <FormItem
          key="price"
          label={getTooltip('Цена Блюда', 'Цена Блюда')}
          name="price"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Цена Блюда`
            }
          ]}
          initialValue={editFood.price}
        >
          <Input type="number" placeholder="Цена" />
        </FormItem>
        <FormItem
          key="category"
          label={getTooltip('Категория ', 'Категория Блюда')}
          name="category"
          rules={[
            {
              required: true,
              message: `Пожалуйста, выберите Категория Блюда`
            }
          ]}
          initialValue={editFood.category}
        >
          <Select mode="single" placeholder="выберите Категорию" showSearch>
            {categories &&
              categories.map(category => (
                <Select.Option
                  key={category.identifier}
                  value={category.identifier}
                >
                  {category.name}
                </Select.Option>
              ))}
          </Select>
        </FormItem>
        <FormItem
          key="proteins"
          label={getTooltip('Белки в Блюде', 'Белки в Блюде')}
          name="proteins"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Белки в Блюде`
            }
          ]}
          initialValue={editFood.proteins}
        >
          <Input type="number" placeholder="Белки в Блюде" />
        </FormItem>
        <FormItem
          key="fats"
          label={getTooltip('Жиры в Блюде', 'Жиры в Блюде')}
          name="fats"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Жиры в Блюде`
            }
          ]}
          initialValue={editFood.fats}
        >
          <Input type="number" placeholder="Жиры в Блюде" />
        </FormItem>
        <FormItem
          key="carbohydrates"
          label={getTooltip('Углеводы в Блюде', 'Углеводы в Блюде')}
          name="carbohydrates"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Углеводы в Блюде`
            }
          ]}
          initialValue={editFood.carbohydrates}
        >
          <Input type="number" placeholder="Углеводы в Блюде" />
        </FormItem>
        <FormItem
          key="calories"
          label={getTooltip('Калории в Блюде', 'Калории в Блюде')}
          name="calories"
          rules={[
            {
              required: true,
              message: `Пожалуйста, напишите Калории в Блюде`
            }
          ]}
          initialValue={editFood.calories}
        >
          <Input type="number" placeholder="Калории в Блюде" />
        </FormItem>
        <FormItem
          key="isActive"
          label={getTooltip('Показывать?', 'Показывать ли блюдо в аппе ?')}
          name="isActive"
          valuePropName="isActive"
        >
          <Switch
            defaultChecked={editFood?.isActive}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
        <FormItem
          key="imageUrl"
          label={getTooltip('Фото', 'Фото Блюда')}
          name="imageUrl"
          rules={[
            {
              required: false,
              message: `Пожалуйста, загрузите Фото`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setImageUrl(value)
              }}
            />
            <StyledAvatar size="96" shape="square" src={imageUrl} />
          </ImageUploadContainer>
        </FormItem>
      </Form>
    </Modal>
  )
}

EditFood.propTypes = {
  editFood: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditFood
