import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'

import Restaurant from './Restaurant.design'
import Loading from '../shared/Loading'
import { useLoading } from '../../context/useLoading'
import withMainLayout from '../../hocs/withMainLayout'

const GET_DATA = gql`
  query getData($id: ID!) {
    restaurant(id: $id) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`
const GET_RESTAURANTS = gql`
  query getRestaurants {
    restaurants {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const UPDATE_RESTAURANT = gql`
  mutation updateRestaurant($id: ID!, $input: RestaurantInput) {
    updateRestaurant(id: $id, input: $input) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`
const DELETE_RESTAURANT = gql`
  mutation deleteRestaurant($id: ID!) {
    deleteRestaurant(id: $id)
  }
`

const RestaurantContainer = props => {
  const { id: restaurantId } = props.match.params
  const history = useHistory()
  const [restaurantData, setRestaurantData] = React.useState(null)
  const { showLoading, hideLoading } = useLoading()

  const { data, loading, error } = useQuery(GET_DATA, {
    variables: { id: restaurantId }
  })
  const [updateRestaurant] = useMutation(UPDATE_RESTAURANT)

  const [deleteRestaurant] = useMutation(DELETE_RESTAURANT)

  useEffect(() => {
    if (data && data.restaurant) {
      setRestaurantData(data.restaurant)
    } else if (error) {
      console.log('error', error)
    }
  }, [data, loading, error])

  if (loading && !restaurantData)
    return (
      <div>
        <Loading />
      </div>
    )

  const handleUpdateClick = (restaurantId, values) => {
    toast.success('Заведение успешно отредактировано')
    updateRestaurant({ variables: { id: restaurantId, input: values } })
  }

  const handleDeleteClick = restaurantId => {
    showLoading()
    deleteRestaurant({ variables: { id: restaurantId } })
    hideLoading()
    toast.success('Заведение успешно удалено')
    history.push('/restaurants')
  }
  return (
    <>
      {restaurantData && (
        <Restaurant
          restaurant={restaurantData}
          updateClick={handleUpdateClick}
          deleteClick={handleDeleteClick}
        />
      )}
    </>
  )
}

RestaurantContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired
}

export default withMainLayout(RestaurantContainer)
