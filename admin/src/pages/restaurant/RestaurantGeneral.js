import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Form, Button, Modal, Descriptions, Input, Avatar, Switch } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'

import { useAuth } from '../../context/useAuth'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'
import EditRestaurant from './EditRestaurant'

const FormItem = Form.Item

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const StyledButton = styled(Button)`
  width: 320px;
  margin-top: 14px;
`
const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const RestaurantGeneral = ({ restaurant, updateClick, deleteClick }) => {
  const editors = ['admin']
  const [form] = Form.useForm()
  const [editModal, setEditModal] = React.useState(false)
  const [imageUrl, setImageUrl] = React.useState(restaurant.imageURL)
  const { getRole } = useAuth()
  const role = getRole()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const renderField = (field, index) => {
    const backgroundColor = index % 2 === 0 ? '#fafafa' : '#ebebeb'
    const labelStyle = { width: '200px', backgroundColor }
    if (typeof restaurant[field.value] === 'boolean') {
      return (
        <Descriptions.Item
          label={field.label}
          labelStyle={labelStyle}
          key={field.label}
        >
          {restaurant[field.value] === false ? 'Нет' : 'Да'}
        </Descriptions.Item>
      )
    }
    if (field.isImage) {
      return (
        <Descriptions.Item label={field.label} key={field.label}>
          <StyledImg src={restaurant[field.value]} alt="No Icon" />
        </Descriptions.Item>
      )
    }

    return (
      <Descriptions.Item
        label={field.label}
        labelStyle={labelStyle}
        key={field.label}
      >
        {restaurant[field.value]}
      </Descriptions.Item>
    )
  }

  const fields = [
    { value: 'imageUrl', label: 'Фото', isImage: true },
    { value: 'name', label: 'Название' },
    { value: 'phoneNumber', label: 'Телефон' },
    { value: 'rating', label: 'Рейтинг' },
    { value: 'priceRating', label: 'Рейтинг по цене' },
    { value: 'isActive', label: 'Показывается ли в аппе(?)' }
  ]

  const handleUpdate = (id, values) => {
    updateClick(id, {
      ...values,
      isActive:
        typeof values.isActive !== 'undefined'
          ? values.isActive
          : restaurant.isActive,
      rating: parseInt(values.rating),
      priceRating: parseInt(values.priceRating)
    })

    setEditModal(false)
  }

  const handleDelete = id => {
    deleteClick(id)
    setEditModal(false)
  }

  return (
    <MainContainer>
      <ButtonContainer>
        <h2> Основные </h2>
        {editors.includes(role) && (
          <Button type="primary" onClick={() => setEditModal(true)}>
            Редактировать
          </Button>
        )}
      </ButtonContainer>
      <Descriptions
        title="Информация о Заведении"
        bordered
        column={1}
        size="small"
      >
        {fields.map((field, index) => {
          return renderField(field, index)
        })}
      </Descriptions>
      {editModal && (
        <EditRestaurant
          visible={editModal}
          onCancel={() => {
            setImageUrl(null)
            setEditModal(null)
          }}
          title="Редактировать Заведение"
          editRestaurant={restaurant}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
        />
      )}
    </MainContainer>
  )
}

RestaurantGeneral.propTypes = {
  restaurant: PropTypes.shape({}).isRequired,
  updateClick: PropTypes.func.isRequired
}

export default RestaurantGeneral
