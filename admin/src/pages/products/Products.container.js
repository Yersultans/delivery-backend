import React, { useState, useEffect } from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import Products from './Products.design'
import Loading from '../shared/Loading'
import { useLoading } from '../../context/useLoading'
import withMainLayout from '../../hocs/withMainLayout'

const GET_PRODUCT = gql`
  query getProducts {
    products {
      id
      imageUrl
      name
      description
      category
      proteins
      fats
      carbohydrates
      calories
    }
  }
`

const GET_CATEGORIES = gql`
  query getCategories {
    categories {
      id
      name
      identifier
    }
  }
`

const DELETE_PRODUCT = gql`
  mutation deleteProduct($id: ID!) {
    deleteProduct(id: $id)
  }
`

const ADD_PRODUCT = gql`
  mutation addProduct($input: ProductInput) {
    addProduct(input: $input) {
      id
      imageUrl
      name
      description
      category
      proteins
      fats
      carbohydrates
      calories
    }
  }
`

const UPDATE_PRODUCT = gql`
  mutation updateProduct($id: ID!, $input: ProductInput) {
    updateProduct(id: $id, input: $input) {
      id
      imageUrl
      name
      description
      category
      proteins
      fats
      carbohydrates
      calories
    }
  }
`

const ProductsContainer = () => {
  const [dataProducts, setDataProducts] = useState([])
  const [dataCategories, setDataCategories] = useState([])
  const { showLoading, hideLoading } = useLoading()

  const [updateProduct] = useMutation(UPDATE_PRODUCT, {
    onCompleted: () => {
      refetch()
    }
  })

  const { data, loading, error, refetch } = useQuery(GET_PRODUCT)

  const {
    data: caloriesData,
    loading: caloriesLoading,
    error: caloriesError
  } = useQuery(GET_CATEGORIES)

  const [deleteProduct] = useMutation(DELETE_PRODUCT, {
    update(cache, { data: { deleteProduct: id } }) {
      const { products } = cache.readQuery({ query: GET_PRODUCT })
      cache.writeQuery({
        query: GET_PRODUCT,
        data: {
          products: products.filter(product => product.id !== id)
        }
      })
      hideLoading()
      toast.success('Product successfully deleted')
    },
    onCompleted: () => {
      refetch()
    }
  })

  const [addProduct] = useMutation(ADD_PRODUCT, {
    update(cache, { data: { addProduct: product } }) {
      const { products } = cache.readQuery({ query: GET_PRODUCT })
      cache.writeQuery({
        query: GET_PRODUCT,
        data: { products: products.concat([product]) }
      })
      hideLoading()
      toast.success('Product successfully created')
    }
  })

  useEffect(() => {
    if (caloriesData && !caloriesLoading) {
      setDataCategories(caloriesData.categories)
    }
  }, [caloriesData, caloriesLoading, caloriesError])

  useEffect(() => {
    if (data && !loading && !error) {
      setDataProducts(data.products)
    }
  }, [data, loading, error])

  const handleDeleteProduct = productId => {
    showLoading()
    deleteProduct({ variables: { id: productId } })
  }

  const handleAddProduct = values => {
    showLoading()
    addProduct({
      variables: {
        input: values
      }
    })
  }

  const handleUpdateProduct = (id, values) => {
    toast.success('Product successfully updated')
    updateProduct({ variables: { id, input: values } })
  }

  if (loading && !dataCategories && !dataProducts) {
    return <Loading />
  }

  return (
    <Products
      products={dataProducts}
      categories={dataCategories}
      deleteProductClick={handleDeleteProduct}
      addProductClick={handleAddProduct}
      updateProductClick={handleUpdateProduct}
    />
  )
}

export default withMainLayout(ProductsContainer)
