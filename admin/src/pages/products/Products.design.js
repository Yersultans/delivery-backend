import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import EditProduct from './EditProduct'

const FormItem = Form.Item

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`
const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`
const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`
const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const Products = ({
  products,
  categories,
  deleteProductClick,
  addProductClick,
  updateProductClick
}) => {
  const [form] = Form.useForm()
  const [display, setDisplay] = useState('default')
  const [editProduct, setEditProduct] = useState(null)
  const [imageUrl, setImageUrl] = useState(null)
  const [modalVisible, setModalVisible] = useState(false)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const columns = [
    {
      title: 'Photo',
      dataIndex: 'imageUrl',
      width: '15%',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Photo" />
        ) : (
          <FeilDiv>No Photo</FeilDiv>
        )
      }
    },
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Сategory',
      dataIndex: 'category'
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setDisplay('edit')
              setImageUrl(item.imageUrl)
              setEditProduct(item)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const editFields = [
    {
      key: 'name',
      label: 'Name',
      value: editProduct && editProduct.name,
      tooltipText: 'Name'
    },
    {
      key: 'category',
      label: 'Category',
      value: editProduct && editProduct.category,
      tooltipText: 'category'
    }
  ]

  const handleCreate = values => {
    addProductClick({
      ...values,
      proteins: parseInt(values.proteins),
      fats: parseInt(values.fats),
      carbohydrates: parseInt(values.carbohydrates),
      calories: parseInt(values.calories)
    })
    setImageUrl(null)
    setModalVisible(false)
  }

  const handleUpdate = values => {
    updateProductClick(editProduct.id, {
      ...values,
      proteins: parseInt(values.proteins),
      fats: parseInt(values.fats),
      carbohydrates: parseInt(values.carbohydrates),
      calories: parseInt(values.calories)
    })
    setImageUrl(null)
    setDisplay('default')
    setEditProduct(null)
  }

  const handleDelete = () => {
    setImageUrl(null)
    deleteProductClick(editProduct.id)
    setDisplay('default')
    setEditProduct(null)
  }

  return (
    <>
      <Table
        dataSource={products}
        columns={columns}
        title={() => (
          <div>
            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New Product
            </Button>
          </div>
        )}
      />

      <Modal
        visible={modalVisible}
        title="New Product"
        okText="Create"
        cancelText="Cancel"
        key="create"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              handleCreate({ ...values, imageUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Product name', 'Product name')}
            name="name"
            rules={[
              {
                required: true,
                message: `Please write Product Name`
              }
            ]}
          >
            <Input placeholder="Name" />
          </FormItem>
          <FormItem
            key="description"
            label={getTooltip('Product Description', 'Product Description')}
            name="description"
            rules={[
              {
                required: true,
                message: `Please write Product Description`
              }
            ]}
          >
            <Input placeholder="Description" />
          </FormItem>
          <FormItem
            key="category"
            label={getTooltip('Product Category', 'Product Category')}
            name="category"
            rules={[
              {
                required: true,
                message: `Please write Product Category`
              }
            ]}
          >
            <Select mode="single" placeholder="Select a category" showSearch>
              {categories &&
                categories.map(category => (
                  <Select.Option
                    key={category.identifier}
                    value={category.identifier}
                  >
                    {category.name}
                  </Select.Option>
                ))}
            </Select>
          </FormItem>
          <FormItem
            key="proteins"
            label={getTooltip('Product Proteins', 'Product Proteins')}
            name="proteins"
          >
            <Input type="number" placeholder="Product Proteins" />
          </FormItem>
          <FormItem
            key="fats"
            label={getTooltip('Product Fats', 'Product Fats')}
            name="fats"
          >
            <Input type="number" placeholder="Product Fats" />
          </FormItem>
          <FormItem
            key="carbohydrates"
            label={getTooltip('Product Carbohydrates', 'Product Carbohydrates')}
            name="carbohydrates"
          >
            <Input type="number" placeholder="Product Carbohydrates" />
          </FormItem>
          <FormItem
            key="calories"
            label={getTooltip('Product Calories', 'Product Calories')}
            name="calories"
          >
            <Input type="number" placeholder="Product Calories" />
          </FormItem>
          <FormItem
            key="imageUrl"
            label={getTooltip('Photo', 'Product Photo')}
            name="imageUrl"
            rules={[
              {
                required: false,
                message: `Please write Product Photo`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  console.log('value', value)
                  setImageUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={imageUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
      {editProduct && (
        <EditProduct
          visible={display === 'edit'}
          onCancel={() => {
            setImageUrl(null)
            setDisplay('default')
            setEditProduct(null)
          }}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
          title="Edit Product"
          categories={categories}
          editProduct={editProduct}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
        />
      )}
    </>
  )
}

Products.propTypes = {
  categories: PropTypes.shape({
    name: PropTypes.string,
    identifier: PropTypes.string
  }).isRequired,
  deleteProductClick: PropTypes.func.isRequired,
  addProductClick: PropTypes.func.isRequired,
  updateProductClick: PropTypes.func.isRequired
}

export default Products
