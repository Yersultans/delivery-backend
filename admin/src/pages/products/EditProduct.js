import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditProduct = ({
  visible,
  onCancel,
  title,
  editProduct,
  categories,
  imageUrl,
  setImageUrl,
  onUpdate,
  onDelete
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editProduct.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button onClick={onCancel}>Cancel</Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate({ ...values, imageUrl })
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Product Name', 'Product Name')}
          name="name"
          rules={[
            {
              required: true,
              message: `Please write Product Name`
            }
          ]}
          initialValue={editProduct.name}
        >
          <Input placeholder="Name" />
        </FormItem>

        <FormItem
          key="description"
          label={getTooltip('Product Description', 'Product Description')}
          name="description"
          rules={[
            {
              required: true,
              message: `Please write Product Description`
            }
          ]}
          initialValue={editProduct.description}
        >
          <Input placeholder="Description" />
        </FormItem>
        <FormItem
          key="category"
          label={getTooltip('Product Category', 'Product Category')}
          name="category"
          rules={[
            {
              required: true,
              message: `Please write Product Category`
            }
          ]}
          initialValue={editProduct.category}
        >
          <Select mode="single" placeholder="Select a category" showSearch>
            {categories &&
              categories.map(category => (
                <Select.Option
                  key={category.identifier}
                  value={category.identifier}
                >
                  {category.name}
                </Select.Option>
              ))}
          </Select>
        </FormItem>
        <FormItem
          key="proteins"
          label={getTooltip('Product Proteins', 'Product Proteins')}
          name="proteins"
          initialValue={editProduct.proteins}
        >
          <Input type="number" placeholder="Product Proteins" />
        </FormItem>
        <FormItem
          key="fats"
          label={getTooltip('Product Fats', 'Product Fats')}
          name="fats"
          initialValue={editProduct.fats}
        >
          <Input type="number" placeholder="Product Fats" />
        </FormItem>
        <FormItem
          key="carbohydrates"
          label={getTooltip('Product Carbohydrates', 'Product Carbohydrates')}
          name="carbohydrates"
          initialValue={editProduct.carbohydrates}
        >
          <Input type="number" placeholder="Product Carbohydrates" />
        </FormItem>
        <FormItem
          key="calories"
          label={getTooltip('Product Calories', 'Product Calories')}
          name="calories"
          initialValue={editProduct.calories}
        >
          <Input type="number" placeholder="Product Calories" />
        </FormItem>
        <FormItem
          key="imageUrl"
          label={getTooltip('Photo', 'Product Photo')}
          name="imageUrl"
          rules={[
            {
              required: false,
              message: `Please write Product Photo`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setImageUrl(value)
              }}
            />
            <StyledAvatar size="96" shape="square" src={imageUrl} />
          </ImageUploadContainer>
        </FormItem>
      </Form>
    </Modal>
  )
}

EditProduct.propTypes = {
  editProduct: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditProduct
