import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer } from 'react-toastify'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  concat
} from '@apollo/client'
import 'typeface-roboto'

import './App.css'
import Login from './auth/Login'
import Users from './users/Users.container'
import User from './user/User.container'

import Restaurants from './restaurants/Restaurants.container'
import Restaurant from './restaurant/Restaurant.contneiner'

import Categories from './categories/Categories.container'

import Shops from './shops/Shops.container'
import Shop from './shop/Shop.container'

import Products from './products/Products.container'

import ShopGeneralContainer from './shopStaff/shopGeneral/ShopGeneral.container'
import ShopOrdersContainer from './shopStaff/shopOrders/ShopOrders.constainer'
import ShopUsersContainer from './shopStaff/shopUsers/ShopUsers.container'
import ShopProductsContainer from './shopStaff/shopProducts/ShopProducts.container'

import PublicPage from './PublicPage'

import withHelmet from '../hocs/withHelmet'
import { ProvideAuth } from '../context/useAuth'
import { ProvideLoading } from '../context/useLoading'
import LoadingDialog from '../context/LoadingDialog'
import PrivateRoute from '../hocs/PrivateRoute'

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_GRAPHQL_URL
})

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('token')
        ? `Bearer ${localStorage.getItem('token')}`
        : null
    }
  })

  return forward(operation)
})

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink)
})

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <ProvideAuth>
        <ProvideLoading>
          <BrowserRouter>
            <ToastContainer />
            <LoadingDialog />
            <Route exact path="/public" component={PublicPage} />
            <Route exact path="/" component={PublicPage} />
            <Route exact path="/login" component={Login} />
            <PrivateRoute
              exact
              path="/users"
              component={Users}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/users/:id"
              component={User}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/restaurants"
              component={Restaurants}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/restaurants/:id"
              component={Restaurant}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/categories"
              component={Categories}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/products"
              component={Products}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/shops"
              component={Shops}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/shops/:id"
              component={Shop}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/generalShop"
              component={ShopGeneralContainer}
              roles={['admin', 'shopStaff']}
            />
            <PrivateRoute
              exact
              path="/ordersShop"
              component={ShopOrdersContainer}
              roles={['admin', 'shopStaff']}
            />
            <PrivateRoute
              exact
              path="/usersShop"
              component={ShopUsersContainer}
              roles={['admin', 'shopStaff']}
            />
            <PrivateRoute
              exact
              path="/shopProducts"
              component={ShopProductsContainer}
              roles={['admin', 'shopStaff']}
            />
          </BrowserRouter>
        </ProvideLoading>
      </ProvideAuth>
    </ApolloProvider>
  )
}

const EnhancedApp = withHelmet([
  { tag: 'title', content: 'Admin | Delivery-app' }
])(App)

export default EnhancedApp
