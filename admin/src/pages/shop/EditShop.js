import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditShop = ({
  visible,
  onCancel,
  title,
  editShop,
  onUpdate,
  onDelete,
  imageUrl,
  setImageUrl
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Create"
      cancelText="Close"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editShop.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Close
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate(editShop.id, values)
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Product Name', 'Product Name')}
          name="name"
          rules={[
            {
              required: true,
              message: `Please write Product Name`
            }
          ]}
          initialValue={editShop.name}
        >
          <Input placeholder="Name" />
        </FormItem>
        <FormItem
          key="phoneNumber"
          label={getTooltip('Product Phone Number', 'Product Phone Number')}
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: `Please write Product Phone Number`
            }
          ]}
          initialValue={editShop.phoneNumber}
        >
          <Input placeholder="Phone Number" />
        </FormItem>
        <Form.Item
          label="Address"
          name="address"
          initialValue={{
            street: editShop.address.street,
            entrance: editShop.address.entrance,
            apartment: editShop.address.apartment,
            intercom: editShop.address.intercom,
            floor: editShop.address.floor
          }}
        >
          <Input.Group compact>
            <Form.Item name={['address', 'street']} noStyle>
              <Input style={{ width: '100%' }} placeholder="Street" />
            </Form.Item>
            <Form.Item name={['address', 'entrance']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Entrance" />
            </Form.Item>
            <Form.Item name={['address', 'apartment']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Apartment" />
            </Form.Item>
            <Form.Item name={['address', 'intercom']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Intercom" />
            </Form.Item>
            <Form.Item name={['address', 'floor']} noStyle>
              <Input style={{ width: '25%' }} placeholder="Floor" />
            </Form.Item>
          </Input.Group>
        </Form.Item>
        <FormItem
          key="rating"
          label={getTooltip('Product Rating', 'Product Rating')}
          name="rating"
          rules={[
            {
              required: true,
              message: `Please write Product Rating`
            }
          ]}
          initialValue={editShop.rating}
        >
          <Input type="number" placeholder="Rating" />
        </FormItem>
        <FormItem
          key="priceRating"
          label={getTooltip('Product Price Rating', 'Product Price Rating')}
          name="priceRating"
          rules={[
            {
              required: true,
              message: `Please write Product Price Rating`
            }
          ]}
          initialValue={editShop.priceRating}
        >
          <Input type="number" placeholder="Price Rating" />
        </FormItem>
        <FormItem
          key="imageUrl"
          label={getTooltip('Product Photo', 'Product Photo')}
          name="imageUrl"
          rules={[
            {
              required: false,
              message: `Please write Product Photo`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setImageUrl(value)
              }}
            />
            <StyledAvatar
              size="96"
              shape="square"
              src={imageUrl || editShop.imageUrl}
            />
          </ImageUploadContainer>
        </FormItem>
        <FormItem
          key="isActive"
          label={getTooltip('Show?', 'Should the product be shown in the app?')}
          name="isActive"
          valuePropName="isActive"
        >
          <Switch
            defaultChecked={editShop?.isActive}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
      </Form>
    </Modal>
  )
}

EditShop.propTypes = {
  editShop: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditShop
