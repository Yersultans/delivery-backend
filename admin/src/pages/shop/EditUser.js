import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Modal,
  Form,
  Input,
  Select,
  Switch,
  Avatar,
  DatePicker
} from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'
import moment from 'moment'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditUser = ({
  visible,
  onCancel,
  title,
  editUser,
  onUpdate,
  onDelete,
  avatarUrl,
  setAvatarUrl
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Save"
      cancelText="Close"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editUser.id)
              onCancel()
            })
          }}
        >
          Delete
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Close
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate(editUser.id, values)
                onCancel()
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="username"
          label={getTooltip('User Email', 'User Email')}
          name="username"
          rules={[
            {
              required: true,
              message: `Please write User Email`
            }
          ]}
          initialValue={editUser.username}
        >
          <Input placeholder="Email" />
        </FormItem>
        <FormItem
          key="lastname"
          label={getTooltip('User Lastname', 'User Lastname')}
          name="lastname"
          rules={[
            {
              required: true,
              message: `Please write User Lastname`
            }
          ]}
          initialValue={editUser.lastname}
        >
          <Input placeholder="Lastname" />
        </FormItem>
        <FormItem
          key="firstname"
          label={getTooltip('User Firstname', 'User Firstname')}
          name="firstname"
          rules={[
            {
              required: true,
              message: `Please write User Fastname`
            }
          ]}
          initialValue={editUser.firstname}
        >
          <Input placeholder="Fastname" />
        </FormItem>
        <FormItem
          key="phoneNumber"
          label={getTooltip('User PhoneNumber', 'User PhoneNumber')}
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: `Please write User PhoneNumber`
            }
          ]}
          initialValue={editUser.phoneNumber}
        >
          <Input placeholder="PhoneNumber" />
        </FormItem>
        <FormItem
          key="birthday"
          label={getTooltip('User Birthday', 'User Birthday')}
          name="birthday"
          rules={[
            {
              required: true,
              message: `Please write User Birthday`
            }
          ]}
          initialValue={moment(editUser.birthday)}
        >
          <StyledDatePicker placeholder="Birthday" />
        </FormItem>
        <FormItem
          key="gender"
          label={getTooltip('User Gender', 'User Gender')}
          name="gender"
          rules={[
            {
              required: true,
              message: `Please write User Gender`
            }
          ]}
          initialValue={editUser.gender}
        >
          <Select mode="single" placeholder="select Gender" showSearch>
            <Select.Option key="male" value="male">
              Male
            </Select.Option>
            <Select.Option key="female" value="female">
              Female
            </Select.Option>
          </Select>
        </FormItem>
        <FormItem
          key="Password"
          label={getTooltip('User Password', 'User Password')}
          name="password"
          rules={[
            {
              required: true,
              message: `Please write User Password`
            }
          ]}
          initialValue={editUser.password}
        >
          <Input type="password" placeholder="Password" />
        </FormItem>
        <FormItem
          key="avatarUrl"
          label={getTooltip('User Avatar', 'User Avatar')}
          name="avatarUrl"
          rules={[
            {
              required: false,
              message: `Please write User Avatar`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setAvatarUrl(value)
              }}
            />
            <StyledAvatar
              size="96"
              shape="square"
              src={avatarUrl || editUser.avatarUrl}
            />
          </ImageUploadContainer>
        </FormItem>
      </Form>
    </Modal>
  )
}

EditUser.propTypes = {
  editUser: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditUser
