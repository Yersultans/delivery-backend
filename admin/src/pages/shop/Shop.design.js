import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Layout, Menu, Breadcrumb, Button, Select, Form, Input } from 'antd'
import { Link } from 'react-router-dom'
import {
  InfoCircleOutlined,
  GatewayOutlined,
  UnorderedListOutlined,
  UserOutlined
} from '@ant-design/icons'
import EmailEditor from 'react-email-editor'

import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

import ShopGeneral from './ShopGeneral'
import ShopProductsContainer from './ShopProducts.container'
import ShopOrdersContainer from './ShopOrders.container'
import ShopUsersContainer from './ShopUsers.container'

const FormItem = Form.Item

const { Header, Sider, Content } = Layout

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const StyledHeader = styled(Header)`
  background: #fff;
  border-bottom: 1px solid #d9d9d9;
  padding: 0px 24px;
`

const StyledMenu = styled(Menu)`
  height: 100%;
  padding-top: 16px;
`
const StyledLayout = styled(Layout)`
  padding: 0 24px 24px;
  background: #fff;
`

const StyledContent = styled(Content)`
  padding: 24px;
  margin: 0px;
  min-height: 280px;
`

const Shop = ({ shop, updateClick, deleteClick }) => {
  const [currentTab, setCurrentTab] = useState(1)
  const [type, setType] = useState(shop.type)

  const handleTabChange = activeKey => {
    setCurrentTab(activeKey)
  }

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Layout>
      <StyledBreadcrumb>
        <Breadcrumb.Item>
          <Link to="/shops">Shops</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{shop?.title}</Breadcrumb.Item>
      </StyledBreadcrumb>
      <StyledHeader>
        <h2>Edit Product</h2>
      </StyledHeader>
      <Layout>
        <Sider>
          <StyledMenu mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item
              icon={<InfoCircleOutlined />}
              key="1"
              onClick={() => handleTabChange(1)}
            >
              Main
            </Menu.Item>
            <Menu.Item
              icon={<GatewayOutlined />}
              key="2"
              onClick={() => handleTabChange(2)}
            >
              Shop Products
            </Menu.Item>
            <Menu.Item
              icon={<UnorderedListOutlined />}
              key="3"
              onClick={() => handleTabChange(3)}
            >
              Shop Orders
            </Menu.Item>
            <Menu.Item
              icon={<UserOutlined />}
              key="4"
              onClick={() => handleTabChange(4)}
            >
              Shop Staff
            </Menu.Item>
          </StyledMenu>
        </Sider>
        <StyledLayout>
          <StyledContent className="site-layout-background">
            {currentTab === 1 && (
              <ShopGeneral
                shop={shop}
                updateClick={updateClick}
                deleteClick={deleteClick}
              />
            )}
            {currentTab === 2 && <ShopProductsContainer shopId={shop.id} />}
            {currentTab === 3 && <ShopOrdersContainer shopId={shop.id} />}
            {currentTab === 4 && <ShopUsersContainer shopId={shop.id} />}
          </StyledContent>
        </StyledLayout>
      </Layout>
    </Layout>
  )
}
Shop.propTypes = {
  shop: PropTypes.shape({}).isRequired,
  updateClick: PropTypes.func.isRequired,
  deleteClick: PropTypes.func.isRequired
}
export default Shop
