import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider,
  Tabs
} from 'antd'
import {
  PlusOutlined,
  SearchOutlined,
  CloseOutlined,
  CheckOutlined,
  FolderOutlined,
  PercentageOutlined
} from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import EditShopProduct from './EditShopProduct'
import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const FormItem = Form.Item
const { TabPane } = Tabs

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`
const ShopProducts = ({
  shopProducts,
  activeShopProducts,
  products,
  addShopProductHandler,
  updateShopProductHandler,
  deleteShopProductHandler
}) => {
  const [form] = Form.useForm()
  const [formForEdit] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [display, setDisplay] = React.useState('default')
  const [editShopProduct, setEditShopProduct] = React.useState(null)
  const [isDiscount, setIsDiscount] = React.useState(false)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listShopProducts, setListShopProducts] = React.useState([])

  React.useEffect(() => {
    if (shopProducts && shopProducts.length > 0) {
      const descriptiveShopProducts = shopProducts.map(shopProduct => {
        return {
          ...shopProduct
        }
      })
      let searchArray = descriptiveShopProducts

      if (searchName) {
        searchArray = searchArray.filter(shopProduct => {
          return shopProduct?.product?.name?.toLowerCase().includes(searchName)
        })
      }
      setListShopProducts(searchArray)
    }
  }, [shopProducts, searchName])

  const columns = [
    {
      title: 'Photo',
      dataIndex: 'imageUrl',
      width: '15%',
      render: (item, data) => {
        return data?.product?.imageUrl ? (
          <StyledImg src={data?.product?.imageUrl} alt="No Icon" />
        ) : (
          <FeilDiv>No Photo</FeilDiv>
        )
      }
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (item, data) => {
        return data?.product?.name
      },
      width: '25%'
    },
    {
      title: 'Price',
      dataIndex: 'price',
      width: '15%'
    },
    {
      title: 'Active',
      dataIndex: 'isActive',
      width: '15%',
      render: item => {
        return item ? (
          <SuccessDiv>Active</SuccessDiv>
        ) : (
          <FeilDiv>Inactive</FeilDiv>
        )
      }
    },
    {
      title: 'Action',
      width: '30%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setDisplay('edit')

              const products = shopProducts.find(
                shopProduct => shopProduct.id.toString() === item.id.toString()
              )
              setIsDiscount(products?.isDiscount)
              setEditShopProduct(products)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const handleCreate = values => {
    addShopProductHandler({
      ...values,
      isActive: values.isActive ? true : false,
      isDiscount: values.isDiscount ? true : false,
      oldPrice: parseInt(values.oldPrice),
      price: parseInt(values.price)
    })
    setModalVisible(false)
    setIsDiscount(false)
  }

  const handleUpdate = values => {
    updateShopProductHandler(editShopProduct.id, {
      ...values,
      isActive:
        typeof values.isActive !== 'undefined'
          ? values.isActive
          : editShopProduct.isActive,
      isDiscount:
        typeof values.isDiscount !== 'undefined'
          ? values.isDiscount
          : editShopProduct.isDiscount,
      oldPrice: parseInt(values.oldPrice),
      price: parseInt(values.price)
    })
    setDisplay('default')
    setEditShopProduct(null)
    setIsDiscount(false)
  }

  const handleDelete = id => {
    console.log('id', id)
    setDisplay('default')
    setEditShopProduct(null)
    deleteShopProductHandler(id)
    setIsDiscount(false)
  }

  return (
    <>
      <Tabs onChange={() => {}} type="card">
        <TabPane
          tab={
            <span>
              <FolderOutlined />
              All Products
            </span>
          }
          key="allProducts"
        >
          <Table
            dataSource={listShopProducts}
            columns={columns}
            rowKey={item => item.id}
            title={() => (
              <StyledHeaderContainer>
                <StyledInput
                  prefix={<SearchOutlined />}
                  placeholder="Name"
                  onChange={e => {
                    setSearchName(e.target.value.toLowerCase())
                  }}
                />

                <Button type="primary" onClick={() => setModalVisible(true)}>
                  <PlusOutlined /> New Product
                </Button>
              </StyledHeaderContainer>
            )}
          />
        </TabPane>
        <TabPane
          tab={
            <span>
              <PercentageOutlined />
              Discount
            </span>
          }
          key="discount"
        >
          <Table
            dataSource={activeShopProducts}
            columns={columns}
            rowKey={item => item.id}
            title={() => (
              <StyledHeaderContainer>
                <Button type="primary" onClick={() => setModalVisible(true)}>
                  <PlusOutlined /> New Product
                </Button>
              </StyledHeaderContainer>
            )}
          />
        </TabPane>
      </Tabs>

      <Modal
        visible={modalVisible}
        title="New Product"
        okText="Create"
        cancelText="Cancel"
        key="create"
        onCancel={() => {
          setModalVisible(false)
          setIsDiscount(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              handleCreate({ ...values })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="product"
            label={getTooltip('Product ', 'Product ID')}
            name="product"
            rules={[
              {
                required: true,
                message: `Please write Product ID`
              }
            ]}
          >
            <Select mode="single" placeholder="select Product ID" showSearch>
              {products &&
                products.map(product => (
                  <Select.Option key={product.id} value={product.id}>
                    {product.name}
                  </Select.Option>
                ))}
            </Select>
          </FormItem>
          <FormItem
            key="price"
            label={getTooltip('Product Price', 'Product Price')}
            name="price"
            rules={[
              {
                required: true,
                message: `Please write Product Price`
              }
            ]}
          >
            <Input type="number" placeholder="Product Price" />
          </FormItem>
          <FormItem
            key="isActive"
            label={getTooltip('Show?', 'Whether to show the dish in the app?')}
            name="isActive"
            valuePropName="isActive"
          >
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </FormItem>
          <FormItem
            key="isDiscount"
            label={getTooltip(
              'Discount?',
              'Whether to show the dish in the app?'
            )}
            name="isDiscount"
            valuePropName="isDiscount"
          >
            <Switch
              onChange={checked => setIsDiscount(checked)}
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </FormItem>
          {isDiscount && (
            <FormItem
              key="oldPrice"
              label={getTooltip('Product Old Price', 'Product Old Price')}
              name="oldPrice"
              rules={[
                {
                  required: true,
                  message: `Please write Product Old Price`
                }
              ]}
            >
              <Input type="number" placeholder="Product Old Price" />
            </FormItem>
          )}
        </Form>
      </Modal>
      {editShopProduct && (
        <EditShopProduct
          visible={display === 'edit'}
          onCancel={() => {
            setDisplay('default')
            setEditShopProduct(null)
            setIsDiscount(false)
          }}
          products={products}
          title="Edit Product"
          editShopProduct={editShopProduct}
          setIsDiscount={setIsDiscount}
          isDiscount={isDiscount}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
        />
      )}
    </>
  )
}

ShopProducts.propTypes = {
  shopProducts: PropTypes.arrayOf(PropTypes.object).isRequired,
  addShopProductHandler: PropTypes.func.isRequired,
  updateShopProductHandler: PropTypes.func.isRequired
}

export default ShopProducts
