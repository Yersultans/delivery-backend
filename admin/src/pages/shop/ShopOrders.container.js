import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import ShopOrders from './ShopOrders.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_ORDERS = gql`
  query ordersByShop($shopId: ID) {
    ordersByShop(shopId: $shopId) {
      id
      identifier
      items {
        product {
          id
          name
        }
        shop {
          id
          name
        }
        imageUrl
        name
        description
        price
        category
        proteins
        fats
        carbohydrates
        calories
        isVegan
        quantity
        totalPrice
      }
      orderProgresses {
        type
        shop {
          id
        }
        isCompleted
      }
      status
      paymentMethod
      courier {
        id
        username
        phoneNumber
      }
      buyer {
        id
        username
        phoneNumber
      }
    }
  }
`

const PINK_UP_PRODUCT_FROM_SHOP = gql`
  mutation pickUpProductFromShop($orderId: ID, $shopId: ID) {
    pickUpProductFromShop(orderId: $orderId, shopId: $shopId) {
      id
    }
  }
`

const ShopOrdersContainer = ({ shopId }) => {
  const { showLoading, hideLoading } = useLoading()
  const [allShopOrders, setAllShopOrders] = React.useState([])
  const [dataProducts, setDataProducts] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_ORDERS, {
    variables: {
      shopId: shopId
    }
  })

  const [
    pickUpProductFromShop,
    { error: pickUpProductFromShopError }
  ] = useMutation(PINK_UP_PRODUCT_FROM_SHOP, {
    onCompleted: () => {
      refetch()
    }
  })

  React.useEffect(() => {
    if (data && !loading) {
      console.log('data.ordersByShop', data.ordersByShop)
      setAllShopOrders(data.ordersByShop)
    } else if (error && !loading) {
      console.log('error', error)
    }
  }, [data, loading, error])

  const pickUpProductFromShopHandler = ({ orderId }) => {
    pickUpProductFromShop({
      variables: {
        orderId: orderId,
        shopId: shopId
      }
    })
  }
  if (loading && !allShopOrders) {
    return <Loading />
  }

  return (
    <ShopOrders
      shopId={shopId}
      shopOrders={allShopOrders}
      pickUpProductFromShopHandler={pickUpProductFromShopHandler}
    />
  )
}

export default ShopOrdersContainer
