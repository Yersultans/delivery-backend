import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'

import Shop from './Shop.design'
import Loading from '../shared/Loading'
import { useLoading } from '../../context/useLoading'
import withMainLayout from '../../hocs/withMainLayout'

const GET_DATA = gql`
  query getData($id: ID!) {
    shop(id: $id) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`
const GET_SHOPS = gql`
  query getShops {
    shops {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const UPDATE_SHOP = gql`
  mutation updateShop($id: ID!, $input: ShopInput) {
    updateShop(id: $id, input: $input) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`
const DELETE_SHOP = gql`
  mutation deleteShop($id: ID!) {
    deleteShop(id: $id)
  }
`

const ShopContainer = props => {
  const { id: shopId } = props.match.params
  const history = useHistory()
  const [shopData, setShopData] = React.useState(null)
  const { showLoading, hideLoading } = useLoading()

  const { data, loading, error } = useQuery(GET_DATA, {
    variables: { id: shopId }
  })
  const [updateShop] = useMutation(UPDATE_SHOP)

  const [deleteShop] = useMutation(DELETE_SHOP)

  useEffect(() => {
    if (data && data.shop) {
      setShopData(data.shop)
    } else if (error) {
      console.log('error', error)
    }
  }, [data, loading, error])

  if (loading && !shopData)
    return (
      <div>
        <Loading />
      </div>
    )

  const handleUpdateClick = (shopId, values) => {
    toast.success('Product successfully edited')
    updateShop({ variables: { id: shopId, input: values } })
  }

  const handleDeleteClick = shopId => {
    showLoading()
    deleteShop({ variables: { id: shopId } })
    hideLoading()
    toast.success('Product removed successfully')
    history.push('/shops')
  }
  return (
    <>
      {shopData && (
        <Shop
          shop={shopData}
          updateClick={handleUpdateClick}
          deleteClick={handleDeleteClick}
        />
      )}
    </>
  )
}

ShopContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired
}

export default withMainLayout(ShopContainer)
