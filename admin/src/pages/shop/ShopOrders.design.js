import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import {
  PlusOutlined,
  SearchOutlined,
  CloseOutlined,
  CheckOutlined,
  FastBackwardFilled
} from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import EditShopProduct from './EditShopProduct'
import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const confirmModal = Modal.confirm

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`

const OrderItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`
const showConfirm = func => {
  confirmModal({
    title: 'Сourier received the order?',
    content: 'Issuance of an order',
    okType: 'primary',
    onOk: () => func(),
    // eslint-disable-next-line no-console 
    onCancel: () => console.log('Cancel'),
    cancelText: 'Cancel',
    okText: 'Ok'
  })
}

const isGaveProducts = ({ orderProgresses, shopId }) => {
  let hasGaveProducts = false
  orderProgresses.forEach(orderProgress => {
    if (orderProgress?.type === 'shop' && orderProgress?.shop?.id === shopId && !orderProgress?.isCompleted) {
      hasGaveProducts = true
    }
  })
  return hasGaveProducts
}

const ShopOrders = ({ shopOrders, shopId, pickUpProductFromShopHandler }) => {
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [editShopOrder, setEditShopOrder] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listShopOrders, setListShopOrders] = React.useState([])

  React.useEffect(() => {
    if (shopOrders && shopOrders.length > 0) {
      const descriptiveShopOrders = shopOrders.map(shopOrder => {
        return {
          ...shopOrder
        }
      })
      let searchArray = descriptiveShopOrders

      if (searchName) {
        searchArray = searchArray.filter(shopOrder => {
          return shopOrder?.identifier?.toLowerCase().includes(searchName)
        })
      }
      setListShopOrders(searchArray)
    }
  }, [shopOrders, searchName])

  const columns = [
    {
      title: 'Number Order',
      dataIndex: 'identifier',
      width: '25%'
    },
    {
      title: 'Buyer',
      render: (item, data) => {
        return data?.buyer?.username
      },
      width: '25%'
    },
    {
      title: 'Price',
      dataIndex: 'price',
      width: '15%'
    },
    {
      title: 'Action',
      width: '30%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setModalVisible(true)
              let order = shopOrders.find(
                shopProduct => shopProduct.id.toString() === item.id.toString()
              )
              setEditShopOrder(order)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  return (
    <>
      <Table
        dataSource={listShopOrders}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Name"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />
          </StyledHeaderContainer>
        )}
      />
      {editShopOrder && (
        <Modal
          visible={modalVisible}
          title="Order Detail"
          key="give"
          onCancel={() => {
            setEditShopOrder(null)
            setModalVisible(false)
          }}
          footer={[
            <Button
              key="submit"
              onClick={() => {
                setEditShopOrder(null)
                setModalVisible(false)
              }}
            >
              Close
            </Button>,
            <Button
              key="submit"
              type="primary"
              disabled={
                !(editShopOrder?.status === 'inProgress' &&
                isGaveProducts({
                  orderProgresses: editShopOrder?.orderProgresses,
                  shopId: shopId
                }))
              }
              onClick={() => {
                if (
                  editShopOrder?.status === 'inProgress' &&
                  isGaveProducts({
                    orderProgresses: editShopOrder?.orderProgresses,
                    shopId: shopId
                  })
                ) {
                  showConfirm(() => {
                    pickUpProductFromShopHandler({ orderId: editShopOrder?.id })
                    setEditShopOrder(null)
                    setModalVisible(false)
                  })
                }
              }}
            >
              Give Order
            </Button>
          ]}
        >
          {editShopOrder &&
            editShopOrder?.items
              .filter(element => element?.shop?.id === shopId)
              .map(element => (
                <OrderItem>
                  <div>{element.name}</div>
                  <div>
                    {element.price} x {element.quantity} = {element.totalPrice}
                  </div>
                </OrderItem>
              ))}
          <OrderItem>
            <div>Total:</div>
            <div>
              {editShopOrder &&
                editShopOrder?.items
                  .filter(element => element?.shop?.id === shopId)
                  .reduce(
                    (previousValue, currentValue) =>
                      previousValue +
                      currentValue?.price * currentValue?.quantity,
                    0
                  )}
            </div>
          </OrderItem>
        </Modal>
      )}
    </>
  )
}

ShopOrders.propTypes = {
  shopOrders: PropTypes.arrayOf(PropTypes.object).isRequired,
  pickUpProductFromShopHandler: PropTypes.func.isRequired
}

export default ShopOrders
