import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import {
  PlusOutlined,
  SearchOutlined,
  CloseOutlined,
  CheckOutlined,
  FastBackwardFilled
} from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const confirmModal = Modal.confirm

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`

const OrderItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`
const showConfirm = func => {
  confirmModal({
    title: 'Сourier received the order?',
    content: 'Issuance of an order',
    okType: 'primary',
    onOk: () => func(),
    // eslint-disable-next-line no-console
    onCancel: () => console.log('Cancel'),
    cancelText: 'Cancel',
    okText: 'Delete'
  })
}

const isGaveProducts = ({ orderProgresses, shopId }) => {
  let hasGaveProducts = false
  orderProgresses.forEach(orderProgress => {
    if (orderProgress.shop.id === shopId) {
      hasGaveProducts = true
    }
  })
  return hasGaveProducts
}

const UserOrders = ({ userOrders }) => {
  console.log('userOrders', userOrders)
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [editUserOrder, setEditUserOrder] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listUserOrders, setListUserOrders] = React.useState([])

  React.useEffect(() => {
    if (userOrders && userOrders.length > 0) {
      const descriptiveUserOrders = userOrders.map(userOrder => {
        return {
          ...userOrder
        }
      })
      let searchArray = descriptiveUserOrders

      if (searchName) {
        searchArray = searchArray.filter(userOrder => {
          return userOrder?.identifier?.toLowerCase().includes(searchName)
        })
      }
      setListUserOrders(searchArray)
    }
  }, [userOrders, searchName])

  const columns = [
    {
      title: 'Number Order',
      dataIndex: 'identifier',
      width: '25%'
    },
    {
      title: 'Total Price',
      dataIndex: 'totalPrice',
      width: '15%'
    },
    {
      title: 'Action',
      width: '30%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setModalVisible(true)
              let order = userOrders.find(
                shopProduct => shopProduct.id.toString() === item.id.toString()
              )
              setEditUserOrder(order)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  return (
    <>
      <Table
        dataSource={listUserOrders}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Number"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />
          </StyledHeaderContainer>
        )}
      />
      {editUserOrder && (
        <Modal
          visible={modalVisible}
          title="Order Detail"
          key="give"
          onCancel={() => {
            setModalVisible(false)
            setEditUserOrder(null)
          }}
        >
          {editUserOrder &&
            editUserOrder?.items.map(element => (
              <OrderItem>
                <div>{element.name}</div>
                <div>
                  {element.price} x {element.quantity} = {element.totalPrice}
                </div>
              </OrderItem>
            ))}
          <OrderItem>
            <div>Total:</div>
            <div>
              {editUserOrder &&
                editUserOrder?.items.reduce(
                  (previousValue, currentValue) =>
                    previousValue +
                    currentValue?.price * currentValue?.quantity,
                  0
                )}
            </div>
          </OrderItem>
        </Modal>
      )}
    </>
  )
}

UserOrders.propTypes = {
  userOrders: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default UserOrders
