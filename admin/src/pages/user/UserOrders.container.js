import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import UserOrders from './UserOrders.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_ORDERS = gql`
  query userOrders($userId: ID) {
    userOrders(userId: $userId) {
      id
      identifier
      items {
        product {
          id
          name
        }
        shop {
          id
          name
        }
        imageUrl
        name
        description
        price
        category
        proteins
        fats
        carbohydrates
        calories
        isVegan
        quantity
        totalPrice
      }
      orderProgresses {
        type
        shop {
          id
        }
        isCompleted
      }
      status
      paymentMethod
      courier {
        id
        username
        phoneNumber
      }
      buyer {
        id
        username
        phoneNumber
      }
      totalPrice
      created_at
    }
  }
`

const UserOrdersContainer = ({ userId }) => {
  const { showLoading, hideLoading } = useLoading()
  const [allUserOrders, setAllUserOrders] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_ORDERS, {
    variables: {
      userId: userId
    }
  })

  React.useEffect(() => {
    if (data && !loading) {
      setAllUserOrders(data.userOrders)
    } else if (error && !loading) {
      console.log('error', error)
    }
  }, [data, loading, error])

  if (loading && !allUserOrders) {
    return <Loading />
  }

  return <UserOrders userOrders={allUserOrders} />
}

export default UserOrdersContainer
