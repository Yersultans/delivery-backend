import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import styled from 'styled-components'

import EditForm from '../shared/EditFormModal'
import CreateForm from '../shared/CreateForm'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import StyledTooltip from '../shared/StyledTooltip'
import EditCategory from './EditCategory'

const FormItem = Form.Item

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`

const Categories = ({
  categories,
  deleteCategoryClick,
  addCategoryClick,
  updateCategoryClick
}) => {
  const [form] = Form.useForm()
  const [display, setDisplay] = useState('default')
  const [editCategory, setEditCategory] = useState(null)
  const [modalVisible, setModalVisible] = useState(false)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Identifier',
      dataIndex: 'identifier'
    },
    {
      title: 'Active',
      dataIndex: 'isActive',
      width: '15%',
      render: item => {
        return item ? (
          <SuccessDiv>Active</SuccessDiv>
        ) : (
          <FeilDiv>Inactive</FeilDiv>
        )
      }
    },
    {
      title: 'Действие',
      key: 'action',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setDisplay('edit')
              setEditCategory(item)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const fields = [
    {
      key: 'name',
      label: 'Name',
      tooltipText: 'Name'
    },
    {
      key: 'identifier',
      label: 'Identifier',
      tooltipText: 'Identifier'
    },
    {
      key: 'isActive',
      label: 'Is Active ?',
      checkbox: true,
      isNotRequired: true,
      tooltipText: 'Is Active ?'
    }
  ]

  const handleCreate = values => {
    addCategoryClick(values)
    setModalVisible(false)
  }

  const handleUpdate = values => {
    console.log('values', values)
    updateCategoryClick(editCategory.id, values)
    setDisplay('default')
    setEditCategory(null)
  }

  const handleDelete = () => {
    deleteCategoryClick(editCategory.id)
    setDisplay('default')
    setEditCategory(null)
  }

  return (
    <>
      <Table
        dataSource={categories}
        columns={columns}
        title={() => (
          <div>
            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New Category
            </Button>
          </div>
        )}
      />

      <Modal
        visible={modalVisible}
        title="New Category"
        okText="Create"
        cancelText="Cancel"
        key="create"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              handleCreate({ ...values })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Category Name', 'Category Name')}
            name="name"
            rules={[
              {
                required: true,
                message: `Please write Category Name`
              }
            ]}
          >
            <Input placeholder="Name" />
          </FormItem>
          <FormItem
            key="identifier"
            label={getTooltip('Category Identifier', 'Category Identifier')}
            name="identifier"
            rules={[
              {
                required: true,
                message: `Please write Category Identifier`
              }
            ]}
          >
            <Input placeholder="identifier" />
          </FormItem>
          <FormItem
            isActive
            label={getTooltip(
              'Show?',
              'Should the product be shown in the app?'
            )}
            name="isActive"
            valuePropName="isActive"
          >
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </FormItem>
        </Form>
      </Modal>
      {editCategory && (
        <EditCategory
          visible={display === 'edit'}
          onCancel={() => {
            setDisplay('default')
            setEditCategory(null)
          }}
          title="Edit Category"
          editCategory={editCategory}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
        />
      )}
    </>
  )
}

Categories.propTypes = {
  categories: PropTypes.shape({
    name: PropTypes.string,
    identifier: PropTypes.string
  }).isRequired,
  deleteCategoryClick: PropTypes.func.isRequired,
  addCategoryClick: PropTypes.func.isRequired,
  updateCategoryClick: PropTypes.func.isRequired
}

export default Categories
