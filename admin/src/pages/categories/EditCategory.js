import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditCategory = ({
  visible,
  onCancel,
  title,
  editCategory,
  onUpdate,
  onDelete
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editCategory.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Cancel
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate(values)
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Category Name', 'Category Name')}
          name="name"
          rules={[
            {
              required: true,
              message: `Please write Category Name`
            }
          ]}
          initialValue={editCategory.name}
        >
          <Input placeholder="Name" />
        </FormItem>
        <FormItem
          key="identifier"
          label={getTooltip('Category Identifier', 'Category Identifier')}
          name="identifier"
          rules={[
            {
              required: true,
              message: `Please write Category Identifier`
            }
          ]}
          initialValue={editCategory.identifier}
        >
          <Input placeholder="identifier" />
        </FormItem>
        <FormItem
          key="isActive"
          label={getTooltip('Show?', 'Should the product be shown in the app?')}
          name="isActive"
          valuePropName="isActive"
        >
          <Switch
            defaultChecked={editCategory?.isActive}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
      </Form>
    </Modal>
  )
}

EditCategory.propTypes = {
  editCategory: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditCategory
