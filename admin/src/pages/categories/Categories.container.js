import React, { useState, useEffect } from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import Categories from './Categories.design'
import Loading from '../shared/Loading'
import { useLoading } from '../../context/useLoading'
import withMainLayout from '../../hocs/withMainLayout'

const GET_CATEGORY = gql`
  query getCategories {
    categories {
      id
      name
      identifier
      isActive
    }
  }
`

const DELETE_CATEGORY = gql`
  mutation deleteCategory($id: ID!) {
    deleteCategory(id: $id)
  }
`

const ADD_CATEGORY = gql`
  mutation addCategory($input: CategoryInput) {
    addCategory(input: $input) {
      id
      name
      identifier
      isActive
    }
  }
`

const UPDATE_CATEGORY = gql`
  mutation updateCategory($id: ID!, $input: CategoryInput) {
    updateCategory(id: $id, input: $input) {
      id
      name
      identifier
      isActive
    }
  }
`

const CategoriesContainer = () => {
  const [dataCategories, setDataCategories] = useState(null)
  const { showLoading, hideLoading } = useLoading()

  const [updateCategory] = useMutation(UPDATE_CATEGORY)

  const { data, loading, error, refetch } = useQuery(GET_CATEGORY)

  const [deleteCategory] = useMutation(DELETE_CATEGORY, {
    update(cache, { data: { deleteCategory: id } }) {
      const { categories } = cache.readQuery({ query: GET_CATEGORY })
      cache.writeQuery({
        query: GET_CATEGORY,
        data: {
          categories: categories.filter(category => category.id !== id)
        }
      })
      hideLoading()
      toast.success('Цена успешно удалена')
    },
    onCompleted: () => {
      refetch()
    }
  })

  const [addCategory] = useMutation(ADD_CATEGORY, {
    update(cache, { data: { addCategory: category } }) {
      const { categories } = cache.readQuery({ query: GET_CATEGORY })
      cache.writeQuery({
        query: GET_CATEGORY,
        data: { categories: categories.concat([category]) }
      })
      hideLoading()
      toast.success('Цена успешно создана')
    }
  })

  useEffect(() => {
    if (data && !loading && !error) {
      setDataCategories(data.categories)
    }
  }, [data, loading, error])

  const handleDeleteCategory = categoryId => {
    showLoading()
    deleteCategory({ variables: { id: categoryId } })
  }

  const handleAddCategory = values => {
    showLoading()
    addCategory({
      variables: {
        input: values
      }
    })
  }

  const handleUpdateCategory = (id, values) => {
    toast.success('Category successfully updated')
    updateCategory({ variables: { id, input: values } })
  }

  if (loading && !dataCategories) {
    return <Loading />
  }

  return (
    <Categories
      categories={dataCategories}
      deleteCategoryClick={handleDeleteCategory}
      addCategoryClick={handleAddCategory}
      updateCategoryClick={handleUpdateCategory}
    />
  )
}

export default withMainLayout(CategoriesContainer)
