import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import Restaurants from './Restaurants.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_RESTAURANTS = gql`
  query restaurants {
    restaurants {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const ADD_RESTAURANT = gql`
  mutation addRestaurant($input: RestaurantInput) {
    addRestaurant(input: $input) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const RestaurantsContainer = () => {
  const { showLoading, hideLoading } = useLoading()
  const [allRestaurants, setAllRestaurants] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_RESTAURANTS)
  const [addRestaurant, { error: errorAddRestaurant }] = useMutation(
    ADD_RESTAURANT,
    {
      update(cache, { data: { addRestaurant: addRestaurantItem } }) {
        const { restaurants } = cache.readQuery({ query: GET_RESTAURANTS })
        cache.writeQuery({
          query: GET_RESTAURANTS,
          data: { restaurants: restaurants.concat([addRestaurantItem]) }
        })
        hideLoading()
        toast.success('Пользователь добавлен')
      }
    }
  )

  React.useEffect(() => {
    if (errorAddRestaurant) {
      console.log(errorAddRestaurant)
    }
  }, [errorAddRestaurant])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllRestaurants(data.restaurants)
    }
  }, [data, loading, error])

  const addRestaurantHandler = value => {
    addRestaurant({
      variables: {
        input: value
      }
    })
  }

  if (loading) {
    return <Loading />
  }

  return (
    <Restaurants
      restaurants={allRestaurants}
      addRestaurant={addRestaurant}
      refetch={refetch}
      addRestaurantHandler={addRestaurantHandler}
    />
  )
}

export default WithMainLayout(RestaurantsContainer)
