import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker
} from 'antd'
import { PlusOutlined, SearchOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`

const Restaurants = ({ restaurants, addRestaurantHandler }) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [imageUrl, setImageUrl] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listRestaurants, setListRestaurants] = React.useState([])

  React.useEffect(() => {
    if (restaurants.length > 0) {
      const descriptiveRestaurants = restaurants.map(restaurant => {
        return {
          ...restaurant
        }
      })
      let searchArray = descriptiveRestaurants

      if (searchName) {
        searchArray = searchArray.filter(restaurant => {
          return restaurant.name?.toLowerCase().includes(searchName)
        })
      }
      setListRestaurants(searchArray)
    }
  }, [restaurants, searchName])

  const columns = [
    {
      title: 'Фото',
      dataIndex: 'imageUrl',
      width: '15%',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Icon" />
        ) : (
          <FeilDiv>Нет Фото</FeilDiv>
        )
      }
    },
    {
      title: 'Название',
      dataIndex: 'name',
      width: '25%'
    },
    {
      title: 'Телефон',
      dataIndex: 'phoneNumber',
      width: '15%'
    },
    {
      title: 'Рейтинг',
      dataIndex: 'rating',
      width: '15%'
    },
    {
      title: 'Активен',
      dataIndex: 'isActive',
      width: '15%',
      render: item => {
        return item ? (
          <SuccessDiv>Активен</SuccessDiv>
        ) : (
          <FeilDiv>Неактивен</FeilDiv>
        )
      }
    },
    {
      title: 'Действие',
      width: '25%',
      render: (text, item) => (
        <span>
          <Link to={`/restaurants/${item.id}`}>Редактировать</Link>
        </span>
      )
    }
  ]

  const handleCreate = values => {
    console.log('values', values)
    addRestaurantHandler({
      ...values,
      rating: parseInt(values.rating),
      priceRating: parseInt(values.priceRating)
    })
    setModalVisible(false)
    setImageUrl(null)
  }

  return (
    <>
      <Table
        dataSource={listRestaurants}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Название"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> Новое заведение
            </Button>
          </StyledHeaderContainer>
        )}
      />
      <Modal
        visible={modalVisible}
        title="Новое заведение"
        okText="Создать"
        cancelText="Отмена"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.imageUrl

              handleCreate({ ...values, imageUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Название заведения', 'Название заведения')}
            name="name"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Название заведения`
              }
            ]}
          >
            <Input placeholder="Название" />
          </FormItem>
          <FormItem
            key="phoneNumber"
            label={getTooltip('Телефон заведения', 'Телефон заведения')}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Телефон заведения`
              }
            ]}
          >
            <Input placeholder="Телефон" />
          </FormItem>
          <Form.Item label="Адрес">
            <Input.Group compact>
              <Form.Item name={['address', 'street']} noStyle>
                <Input style={{ width: '100%' }} placeholder="Улица" />
              </Form.Item>
              <Form.Item name={['address', 'entrance']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Подъезд" />
              </Form.Item>
              <Form.Item name={['address', 'apartment']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Квартира" />
              </Form.Item>
              <Form.Item name={['address', 'intercom']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Домафон" />
              </Form.Item>
              <Form.Item name={['address', 'floor']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Этаж" />
              </Form.Item>
            </Input.Group>
          </Form.Item>
          <FormItem
            key="rating"
            label={getTooltip('Рейтинг заведения', 'Рейтинг заведения')}
            name="rating"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Рейтинг заведения`
              }
            ]}
          >
            <Input type="number" placeholder="Рейтинг" />
          </FormItem>
          <FormItem
            key="priceRating"
            label={getTooltip(
              'Ценавой Рейтинг заведения',
              'Ценавой Рейтинг заведения'
            )}
            name="priceRating"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Ценавой Рейтинг заведения`
              }
            ]}
          >
            <Input type="number" placeholder="Ценавой Рейтинг" />
          </FormItem>
          <FormItem
            key="imageUrl"
            label={getTooltip('Аватарка', 'Аватарка пользователя')}
            name="imageUrl"
            rules={[
              {
                required: false,
                message: `Пожалуйста, загрузите Аватарку`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setImageUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={imageUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
    </>
  )
}

Restaurants.propTypes = {
  restaurants: PropTypes.arrayOf(PropTypes.object).isRequired,
  addRestaurantHandler: PropTypes.func.isRequired
}

export default Restaurants
