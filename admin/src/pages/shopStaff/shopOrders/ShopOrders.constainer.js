import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../../context/useLoading'
import ShopOrders from './ShopOrders'
import Loading from '../../shared/Loading'
import { useAuth } from '../../../context/useAuth'
import withMainLayout from '../../../hocs/withMainLayout'

const GET_ORDERS = gql`
  query ordersByShop($shopId: ID) {
    ordersByShop(shopId: $shopId) {
      id
      identifier
      items {
        product {
          id
          name
        }
        shop {
          id
          name
        }
        imageUrl
        name
        description
        price
        category
        proteins
        fats
        carbohydrates
        calories
        isVegan
        quantity
        totalPrice
      }
      orderProgresses {
        type
        shop {
          id
        }
        isCompleted
      }
      status
      paymentMethod
      courier {
        id
        username
        phoneNumber
      }
      buyer {
        id
        username
        phoneNumber
      }
    }
  }
`

const PINK_UP_PRODUCT_FROM_SHOP = gql`
  mutation pickUpProductFromShop($orderId: ID, $shopId: ID) {
    pickUpProductFromShop(orderId: $orderId, shopId: $shopId) {
      id
    }
  }
`

const ShopOrdersContainer = () => {
  const { showLoading, hideLoading } = useLoading()
  const { user } = useAuth()
  const [allShopOrders, setAllShopOrders] = React.useState([])
  const [dataProducts, setDataProducts] = React.useState([])
  const [shopId, setShopId] = React.useState(null)
  const [getOrders, { data, loading, error, refetch }] = useLazyQuery(
    GET_ORDERS
  )

  React.useEffect(() => {
    if (user && user?.shop?.id) {
      getOrders({
        variables: { shopId: user?.shop?.id }
      })
      setShopId(user?.shop?.id)
    } else if (error) {
      console.log('error', error)
    }
  }, [user])

  const [
    pickUpProductFromShop,
    { error: pickUpProductFromShopError }
  ] = useMutation(PINK_UP_PRODUCT_FROM_SHOP, {
    onCompleted: () => {
      refetch()
    }
  })

  React.useEffect(() => {
    if (data && !loading) {
      setAllShopOrders(data.ordersByShop)
    } else if (error && !loading) {
      console.log('error', error)
    }
  }, [data, loading, error])

  const pickUpProductFromShopHandler = ({ orderId }) => {
    pickUpProductFromShop({
      variables: {
        orderId: orderId,
        shopId: shopId
      }
    })
  }
  if (loading && !allShopOrders) {
    return <Loading />
  }

  return (
    <ShopOrders
      shopId={shopId}
      shopOrders={allShopOrders}
      pickUpProductFromShopHandler={pickUpProductFromShopHandler}
    />
  )
}

export default withMainLayout(ShopOrdersContainer)
