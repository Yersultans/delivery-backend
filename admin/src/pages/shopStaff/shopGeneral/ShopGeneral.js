import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Form, Button, Modal, Descriptions, Input, Avatar, Switch } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'

import { useAuth } from '../../../context/useAuth'

import ImageUpload from '../../shared/ImageUpload'
import StyledTooltip from '../../shared/StyledTooltip'
import showConfirm from '../../shared/DeleteConfirm'
import EditShop from './EditShop'

const FormItem = Form.Item

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: #fff;
  padding: 24px;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const StyledButton = styled(Button)`
  width: 320px;
  margin-top: 14px;
`
const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const ShopGeneral = ({ shop, updateClick }) => {
  const editors = ['admin', 'shopStaff']
  const [form] = Form.useForm()
  const [editModal, setEditModal] = React.useState(false)
  const [imageUrl, setImageUrl] = React.useState(shop.imageURL)
  const { getRole } = useAuth()
  const role = getRole()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const renderField = (field, index) => {
    const backgroundColor = index % 2 === 0 ? '#fafafa' : '#ebebeb'
    const labelStyle = { width: '200px', backgroundColor }
    if (typeof shop[field.value] === 'boolean') {
      return (
        <Descriptions.Item
          label={field.label}
          labelStyle={labelStyle}
          key={field.label}
        >
          {shop[field.value] === false ? 'No' : 'Yes'}
        </Descriptions.Item>
      )
    }
    if (field.isImage) {
      return (
        <Descriptions.Item label={field.label} key={field.label}>
          <StyledImg src={shop[field.value]} alt="No Icon" />
        </Descriptions.Item>
      )
    }

    return (
      <Descriptions.Item
        label={field.label}
        labelStyle={labelStyle}
        key={field.label}
      >
        {shop[field.value]}
      </Descriptions.Item>
    )
  }

  const fields = [
    { value: 'imageUrl', label: 'Photo', isImage: true },
    { value: 'name', label: 'Name' },
    { value: 'phoneNumber', label: 'Phone Number' },
    { value: 'rating', label: 'Rating' },
    { value: 'priceRating', label: 'Price Rating' },
    { value: 'isActive', label: 'Does it show up in the app(?)' }
  ]

  const handleUpdate = (id, values) => {
    updateClick(id, {
      ...values,
      isActive:
        typeof values.isActive !== 'undefined'
          ? values.isActive
          : shop.isActive,
      rating: parseInt(values.rating),
      priceRating: parseInt(values.priceRating)
    })

    setEditModal(false)
  }

  return (
    <MainContainer>
      <ButtonContainer>
        <h2> Main </h2>
        {editors.includes(role) && (
          <Button type="primary" onClick={() => setEditModal(true)}>
            Edit
          </Button>
        )}
      </ButtonContainer>
      <Descriptions title="Shop Information" bordered column={1} size="small">
        {fields.map((field, index) => {
          return renderField(field, index)
        })}
      </Descriptions>
      {editModal && (
        <EditShop
          visible={editModal}
          onCancel={() => {
            setImageUrl(null)
            setEditModal(null)
          }}
          title="Edit Shop"
          editShop={shop}
          onUpdate={handleUpdate}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
        />
      )}
    </MainContainer>
  )
}

ShopGeneral.propTypes = {
  shop: PropTypes.shape({}).isRequired,
  updateClick: PropTypes.func.isRequired
}

export default ShopGeneral
