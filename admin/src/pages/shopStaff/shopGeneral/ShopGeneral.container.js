import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'

import Shop from './ShopGeneral'
import Loading from '../../shared/Loading'
import { useLoading } from '../../../context/useLoading'
import withMainLayout from '../../../hocs/withMainLayout'
import { useAuth } from '../../../context/useAuth'

const GET_DATA = gql`
  query getData($id: ID!) {
    shop(id: $id) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`
const GET_SHOPS = gql`
  query getShops {
    shops {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const UPDATE_SHOP = gql`
  mutation updateShop($id: ID!, $input: ShopInput) {
    updateShop(id: $id, input: $input) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const ShopGeneralContainer = () => {
  const { user } = useAuth()
  const history = useHistory()
  const [shopData, setShopData] = React.useState(null)
  const [shopId, setShopId] = React.useState(null)
  const { showLoading, hideLoading } = useLoading()

  const [getShop, { data, loading, error }] = useLazyQuery(GET_DATA)
  const [updateShop] = useMutation(UPDATE_SHOP)

  useEffect(() => {
    if (user && user?.shop?.id) {
      getShop({
        variables: { id: user?.shop?.id }
      })
      setShopId(user?.shop?.id)
    } else if (error) {
      console.log('error', error)
    }
  }, [user])

  useEffect(() => {
    if (data && data.shop) {
      setShopData(data.shop)
    } else if (error) {
      console.log('error', error)
    }
  }, [data, loading, error])

  if (loading && !shopData)
    return (
      <div>
        <Loading />
      </div>
    )

  const handleUpdateClick = (shopId, values) => {
    toast.success('Product successfully edited')
    updateShop({ variables: { id: shopId, input: values } })
  }

  return (
    <>
      {shopData && (
        <Shop
          shop={shopData}
          updateClick={handleUpdateClick}
          // deleteClick={handleDeleteClick}
        />
      )}
    </>
  )
}

ShopGeneralContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired
}

export default withMainLayout(ShopGeneralContainer)
