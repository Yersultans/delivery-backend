import React, { useState } from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../../context/useLoading'
import ShopProducts from './ShopProducts.design'
import withMainLayout from '../../../hocs/withMainLayout'
import { useAuth } from '../../../context/useAuth'
import Loading from '../../shared/Loading'

const GET_FOODS = gql`
  query shopProductsByShop($shopId: ID) {
    shopProductsByShop(shopId: $shopId) {
      id
      product {
        id
        name
        imageUrl
        category
      }
      price
      oldPrice
      isDiscount
      isActive
    }
  }
`
const GET_PRODUCTS = gql`
  query getProducts {
    products {
      id
      name
      imageUrl
      category
    }
  }
`

const ADD_FOOD = gql`
  mutation addShopProduct($input: ShopProductInput) {
    addShopProduct(input: $input) {
      id
      product {
        id
        name
        imageUrl
        category
      }
      price
      oldPrice
      isDiscount
      isActive
    }
  }
`

const DELETE_FOOD = gql`
  mutation deleteShopProduct($id: ID!) {
    deleteShopProduct(id: $id)
  }
`
const UPDATE_FOOD = gql`
  mutation updateShopProduct($id: ID!, $input: ShopProductInput) {
    updateShopProduct(id: $id, input: $input) {
      id
      product {
        id
        name
        imageUrl
        category
      }
      price
      oldPrice
      isActive
    }
  }
`

const ShopProductsContainer = () => {
  const { showLoading, hideLoading } = useLoading()
	const { user } = useAuth()
	const [shopId, setShopId] = React.useState(null)
  const [allShopProducts, setAllShopProducts] = React.useState([])
  const [dataProducts, setDataProducts] = React.useState([])
  const [activeShopProducts, setActiveShopProducts] = React.useState([])
  const [ getFoods, { data, loading, error, refetch }] = useLazyQuery(GET_FOODS)

  const {
    data: productsData,
    loading: productsLoading,
    error: productsError
  } = useQuery(GET_PRODUCTS)

	React.useEffect(() => {
    if (user && user?.shop?.id) {
      getFoods({
        variables: { shopId: user?.shop?.id }
      })
      setShopId(user?.shop?.id)
    } else if (error) {
      console.log('error', error)
    }
  }, [user])

  const [updateShopProduct] = useMutation(UPDATE_FOOD)
  const [addShopProduct, { error: errorAddShopProduct }] = useMutation(
    ADD_FOOD,
    {
      update(cache, { data: { addShopProduct: addShopProductItem } }) {
        const { shopProductsByShop } = cache.readQuery({
          query: GET_FOODS,
          variables: {
            shopId: shopId
          }
        })
        cache.writeQuery({
          query: GET_FOODS,
          variables: {
            shopId: shopId
          },
          data: {
            shopProductsByShop: shopProductsByShop.concat([addShopProductItem])
          }
        })
        hideLoading()
        toast.success('Product added to the shop')
      }
    }
  )

  const [deleteShopProduct] = useMutation(DELETE_FOOD, {
    update(cache, { data: { deleteShopProduct: id } }) {
      const { shopProductsByShop } = cache.readQuery({
        query: GET_FOODS,
        variables: {
          shopId: shopId
        }
      })
      cache.writeQuery({
        query: GET_FOODS,
        variables: {
          shopId: shopId
        },
        data: {
          shopProductsByShop: shopProductsByShop.filter(shop => shop.id !== id)
        }
      })
      hideLoading()
      toast.success('Product has been successfully removed from the Shop')
    }
  })


  React.useEffect(() => {
    if (productsData && !productsLoading) {
      setDataProducts(productsData.products)
    }
  }, [productsData, productsLoading, productsError])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllShopProducts(data.shopProductsByShop)
      setActiveShopProducts(
        data.shopProductsByShop.filter(shopProduct => shopProduct.isDiscount)
      )
    }
  }, [data, loading, error])

  const addShopProductHandler = value => {
    addShopProduct({
      variables: {
        input: { ...value, shop: shopId }
      }
    })
  }

  const updateShopProductHandler = (id, values) => {
    toast.success('Product successfully edited')
    updateShopProduct({ variables: { id, input: values } })
  }

  const deleteShopProductHandler = id => {
    showLoading()
    deleteShopProduct({ variables: { id: id } })
    hideLoading()
  }

  if (loading && !allShopProducts) {
    return <Loading />
  }

  return (
    <ShopProducts
      shopProducts={allShopProducts}
      products={dataProducts}
      activeShopProducts={activeShopProducts}
      addShopProductHandler={addShopProductHandler}
      updateShopProductHandler={updateShopProductHandler}
      deleteShopProductHandler={deleteShopProductHandler}
    />
  )
}

export default withMainLayout(ShopProductsContainer)
