import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../../shared/ImageUpload'
import StyledTooltip from '../../shared/StyledTooltip'
import showConfirm from '../../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditShopProduct = ({
  visible,
  onCancel,
  title,
  editShopProduct,
  products,
  setIsDiscount,
  isDiscount,
  onUpdate,
  onDelete
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editShopProduct.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button onClick={onCancel}>Cancel</Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate({ ...values })
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="price"
          label={getTooltip('Product Price', 'Product Price')}
          name="price"
          initialValue={editShopProduct.price}
        >
          <Input type="number" placeholder="Product Price" />
        </FormItem>
        <FormItem
          key="product"
          label={getTooltip('Product ID', 'Product ID')}
          name="product"
          rules={[
            {
              required: true,
              message: `Please write Product ID`
            }
          ]}
          initialValue={editShopProduct?.product?.id}
        >
          <Select mode="single" placeholder="Select a category" showSearch>
            {products &&
              products.map(product => (
                <Select.Option key={product.id} value={product.id}>
                  <Avatar size="24" shape="square" src={product?.imageUrl} />
                  {product.name}
                </Select.Option>
              ))}
          </Select>
        </FormItem>
        <FormItem
          key="isActive"
          label={getTooltip('Show?', 'Should the product be shown in the app?')}
          name="isActive"
          valuePropName="isActive"
        >
          <Switch
            defaultChecked={editShopProduct?.isActive}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
        <FormItem
          key="isDiscount"
          label={getTooltip(
            'Discount?',
            'Whether to show the dish in the app?'
          )}
          name="isDiscount"
          valuePropName="isDiscount"
        >
          <Switch
            defaultChecked={editShopProduct?.isDiscount}
            onChange={checked => setIsDiscount(checked)}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
        {isDiscount && (
          <FormItem
            key="oldPrice"
            label={getTooltip('Product Old Price', 'Product Old Price')}
            name="oldPrice"
            rules={[
              {
                required: true,
                message: `Please write Product Old Price`
              }
            ]}
            initialValue={editShopProduct.oldPrice}
          >
            <Input type="number" placeholder="Product Old Price" />
          </FormItem>
        )}
      </Form>
    </Modal>
  )
}

EditShopProduct.propTypes = {
  editProduct: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditShopProduct
