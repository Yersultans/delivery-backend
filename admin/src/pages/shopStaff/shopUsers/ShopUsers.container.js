import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../../context/useLoading'
import ShopUsers from './ShopUsers'
import withMainLayout from '../../../hocs/withMainLayout'
import Loading from '../../shared/Loading'
import { useAuth } from '../../../context/useAuth'

const GET_USERS = gql`
  query getUsers($shopId: ID) {
    usersByShop(shopId: $shopId) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`

const ADD_USER = gql`
  mutation addUser($input: UserInput) {
    addUser(input: $input) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`

const UPDATE_USER = gql`
  mutation updateUser($id: ID!, $input: UserInput) {
    updateUser(id: $id, input: $input) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`
const DELETE_USER = gql`
  mutation deleteUser($id: ID!) {
    deleteUser(id: $id)
  }
`

const ShopUsersContainer = () => {
  const { showLoading, hideLoading } = useLoading()
  const { user } = useAuth()
  const [allUsers, setAllUsers] = React.useState(null)
  const [getUsers, { data, loading, error, refetch }] = useLazyQuery(GET_USERS)
  const [shopId, setShopId] = React.useState(null)
  const [updateUser] = useMutation(UPDATE_USER)

  const [deleteUser] = useMutation(DELETE_USER, {
    update(cache, { data: { deleteUser: id } }) {
      const { usersByShop } = cache.readQuery({
        query: GET_USERS,
        variables: { shopId: shopId }
      })
      cache.writeQuery({
        query: GET_USERS,
        variables: { shopId: shopId },
        data: { usersByShop: usersByShop.filter(user => user.id !== id) }
      })
      hideLoading()
      toast.success('User removed successfully')
    }
  })

  const [addUser, { error: errorAddUser }] = useMutation(ADD_USER, {
    update(cache, { data: { addUser: addUserItem } }) {
      const { usersByShop } = cache.readQuery({
        query: GET_USERS,
        variables: { shopId: shopId }
      })
      cache.writeQuery({
        query: GET_USERS,
        variables: { shopId: shopId },
        data: { usersByShop: usersByShop.concat([addUserItem]) }
      })
      hideLoading()
      toast.success('User Added')
    }
  })

  React.useEffect(() => {
    if (user && user?.shop?.id) {
      getUsers({
        variables: { shopId: user?.shop?.id }
      })
      setShopId(user?.shop?.id)
    } else if (error) {
      console.log('error', error)
    }
  }, [user])

  React.useEffect(() => {
    if (errorAddUser) {
      console.log(errorAddUser)
    }
  }, [errorAddUser])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllUsers(data.usersByShop)
    }
  }, [data, loading, error])

  const addUserHandler = value => {
    addUser({
      variables: {
        input: { ...value, shop: shopId, role: 'shopStaff' }
      }
    })
  }

  const updateUserHandler = (userId, values) => {
    toast.success('User successfully edited')
    updateUser({
      variables: {
        id: userId,
        input: { ...values, shop: shopId, role: 'shopStaff' }
      }
    })
  }

  const deleteUserHandler = userId => {
    showLoading()
    deleteUser({ variables: { id: userId } })
    hideLoading()
    toast.success('User deleted successfully')
  }

  if (loading || !allUsers) {
    return <Loading />
  }

  return (
    <ShopUsers
      users={allUsers}
      addUserHandler={addUserHandler}
      updateUserHandler={updateUserHandler}
      deleteUserHandler={deleteUserHandler}
    />
  )
}

export default withMainLayout(ShopUsersContainer)
