import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Divider
} from 'antd'
import { PlusOutlined, SearchOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../../shared/ImageUpload'
import StyledTooltip from '../../shared/StyledTooltip'
import EditUser from './EditUser'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`
const FeilDiv = styled.div`
  color: red;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const Users = ({
  users,
  addUserHandler,
  updateUserHandler,
  deleteUserHandler
}) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchNickname, setSearchNickname] = React.useState(null)
  const [avatarUrl, setAvatarUrl] = React.useState(null)
  const [editModal, setEditModal] = React.useState(false)
  const [editUser, setEditUser] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listUsers, setListUsers] = React.useState(null)

  React.useEffect(() => {
    const descriptiveUsers = users.map(user => {
      return {
        ...user,
        fullname: `${user?.lastname} ${user?.firstname}`,
        genderName:
          (user.sex === 'male' && 'Male') || (user.sex === 'female' && 'Female')
      }
    })
    let searchArray = descriptiveUsers

    if (searchNickname) {
      searchArray = searchArray.filter(user => {
        return (
          user?.username?.toLowerCase().includes(searchNickname) ||
          user.lastname?.toLowerCase().includes(searchNickname) ||
          user.phoneNumber?.toLowerCase().includes(searchNickname) ||
          user.firstname?.toLowerCase().includes(searchNickname)
        )
      })
    }

    setListUsers(searchArray)
  }, [users, searchNickname])

  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'avatarUrl',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Icon" />
        ) : (
          <FeilDiv>No Photo</FeilDiv>
        )
      }
    },
    {
      title: 'Fullname',
      dataIndex: 'fullname',
      width: '25%'
    },
    {
      title: 'Email',
      dataIndex: 'username',
      width: '15%'
    },
    {
      title: 'PhoneNumber',
      dataIndex: 'phoneNumber',
      width: '15%'
    },
    {
      title: 'Gender',
      dataIndex: 'genderName',
      width: '10%'
    },
    {
      title: 'Действие',
      width: '20%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setEditModal(true)
              setEditUser(item)
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const handleCreate = values => {
    addUserHandler(values)
    setModalVisible(false)
    setAvatarUrl(null)
  }

  return (
    <>
      <Table
        dataSource={listUsers}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="FirstName/LastName/Email/PhoneNumber"
              onChange={e => {
                setSearchNickname(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New User
            </Button>
          </StyledHeaderContainer>
        )}
      />
      <Modal
        visible={modalVisible}
        title="New User"
        okText="Create"
        cancelText="Cancel"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.avatarUrl

              handleCreate({ ...values, avatarUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="username"
            label={getTooltip('User Email', 'User Email')}
            name="username"
            rules={[
              {
                required: true,
                message: `Please write User Email`
              }
            ]}
          >
            <Input placeholder="Email" />
          </FormItem>
          <FormItem
            key="lastname"
            label={getTooltip('User LastName', 'User LastName')}
            name="lastname"
            rules={[
              {
                required: true,
                message: `Please write User LastName`
              }
            ]}
          >
            <Input placeholder="LastName" />
          </FormItem>
          <FormItem
            key="firstname"
            label={getTooltip('User FirstName', 'User FirstName')}
            name="firstname"
            rules={[
              {
                required: true,
                message: `Please write User FirstName`
              }
            ]}
          >
            <Input placeholder="FirstName" />
          </FormItem>
          <FormItem
            key="phoneNumber"
            label={getTooltip('User PhoneNumber', 'User PhoneNumber')}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Please write User PhoneNumber`
              }
            ]}
          >
            <Input placeholder="PhoneNumber" />
          </FormItem>
          <FormItem
            key="gender"
            label={getTooltip('User Gender', 'User Gender')}
            name="gender"
            rules={[
              {
                required: true,
                message: `Please write User Gender`
              }
            ]}
          >
            <Select mode="single" placeholder="select Gender" showSearch>
              <Select.Option key="male" value="male">
                Male
              </Select.Option>
              <Select.Option key="female" value="female">
                Female
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="birthday"
            label={getTooltip('User Birthday', 'User Birthday')}
            name="birthday"
            rules={[
              {
                required: true,
                message: `Please write User Gender`
              }
            ]}
          >
            <StyledDatePicker placeholder="Select User Gender" />
          </FormItem>
          <FormItem
            key="Password"
            label={getTooltip('User Password', 'User Password')}
            name="password"
            rules={[
              {
                required: true,
                message: `Please write User Password`
              }
            ]}
          >
            <Input type="password" placeholder="Password" />
          </FormItem>
          <FormItem
            key="avatarUrl"
            label={getTooltip('User Avatar', 'User Avatar')}
            name="avatarUrl"
            rules={[
              {
                required: false,
                message: `Please write User Avatar`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setAvatarUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={avatarUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
      {editModal && (
        <EditUser
          visible={editModal}
          onCancel={() => {
            setAvatarUrl(null)
            setEditModal(null)
          }}
          title="Edit User"
          editUser={editUser}
          onUpdate={updateUserHandler}
          onDelete={deleteUserHandler}
          avatarUrl={avatarUrl}
          setAvatarUrl={setAvatarUrl}
        />
      )}
    </>
  )
}

Users.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  addUserHandler: PropTypes.func.isRequired
}

export default Users
