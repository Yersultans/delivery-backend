import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker
} from 'antd'
import { PlusOutlined, SearchOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`

const Shops = ({ shops, addShopHandler }) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [imageUrl, setImageUrl] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listShops, setListShops] = React.useState([])

  React.useEffect(() => {
    if (shops.length > 0) {
      const descriptiveShops = shops.map(shop => {
        return {
          ...shop
        }
      })
      let searchArray = descriptiveShops

      if (searchName) {
        searchArray = searchArray.filter(shop => {
          return shop.name?.toLowerCase().includes(searchName)
        })
      }
      setListShops(searchArray)
    }
  }, [shops, searchName])

  const columns = [
    {
      title: 'Photo',
      dataIndex: 'imageUrl',
      width: '15%',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Icon" />
        ) : (
          <FeilDiv>No Photo</FeilDiv>
        )
      }
    },
    {
      title: 'Name',
      dataIndex: 'name',
      width: '25%'
    },
    {
      title: 'PhoneNumber',
      dataIndex: 'phoneNumber',
      width: '15%'
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      width: '15%'
    },
    {
      title: 'Active',
      dataIndex: 'isActive',
      width: '15%',
      render: item => {
        return item ? (
          <SuccessDiv>Active</SuccessDiv>
        ) : (
          <FeilDiv>Inactive</FeilDiv>
        )
      }
    },
    {
      title: 'Action',
      width: '25%',
      render: (text, item) => (
        <span>
          <Link to={`/shops/${item.id}`}>Edit</Link>
        </span>
      )
    }
  ]

  const handleCreate = values => {
    console.log('values', values)
    addShopHandler({
      ...values,
      rating: parseInt(values.rating),
      priceRating: parseInt(values.priceRating)
    })
    setModalVisible(false)
    setImageUrl(null)
  }

  return (
    <>
      <Table
        dataSource={listShops}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Name"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New Shop
            </Button>
          </StyledHeaderContainer>
        )}
      />
      <Modal
        visible={modalVisible}
        title="New Shop"
        okText="Create"
        cancelText="Cancel"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.imageUrl

              handleCreate({ ...values, imageUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Shop Name', 'Shop Name')}
            name="name"
            rules={[
              {
                required: true,
                message: `Please write Shop Name`
              }
            ]}
          >
            <Input placeholder="Name" />
          </FormItem>
          <FormItem
            key="phoneNumber"
            label={getTooltip('Shop PhoneNumber', 'Shop PhoneNumber')}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Please write Shop PhoneNumber`
              }
            ]}
          >
            <Input placeholder="PhoneNumber" />
          </FormItem>
          <Form.Item label="Address">
            <Input.Group compact>
              <Form.Item name={['address', 'street']} noStyle>
                <Input style={{ width: '100%' }} placeholder="Street" />
              </Form.Item>
              <Form.Item name={['address', 'entrance']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Entrance" />
              </Form.Item>
              <Form.Item name={['address', 'apartment']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Apartment" />
              </Form.Item>
              <Form.Item name={['address', 'intercom']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Intercom" />
              </Form.Item>
              <Form.Item name={['address', 'floor']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Floor" />
              </Form.Item>
            </Input.Group>
          </Form.Item>
          <FormItem
            key="rating"
            label={getTooltip('Shop Rating', 'Shop Rating')}
            name="rating"
            rules={[
              {
                required: true,
                message: `Please write Shop PhoneNumber`
              }
            ]}
          >
            <Input type="number" placeholder="Rating" />
          </FormItem>
          <FormItem
            key="priceRating"
            label={getTooltip('Shop Price Rating', 'Shop Price Rating')}
            name="priceRating"
            rules={[
              {
                required: true,
                message: `Please write Shop Price Rating`
              }
            ]}
          >
            <Input type="number" placeholder="Price Rating" />
          </FormItem>
          <FormItem
            key="imageUrl"
            label={getTooltip('Shop Photo', 'Shop Photo')}
            name="imageUrl"
            rules={[
              {
                required: false,
                message: `Please write Shop Photo`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setImageUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={imageUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
    </>
  )
}

Shops.propTypes = {
  shops: PropTypes.arrayOf(PropTypes.object).isRequired,
  addProductHandler: PropTypes.func.isRequired
}

export default Shops
