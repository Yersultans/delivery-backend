import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import Shops from './Shops.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_SHOPS = gql`
  query shops {
    shops {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const ADD_SHOP = gql`
  mutation addShop($input: ShopInput) {
    addShop(input: $input) {
      id
      name
      imageUrl
      phoneNumber
      rating
      priceRating
      address {
        street
        entrance
        apartment
        intercom
        floor
        comment
      }
      isActive
    }
  }
`

const ShopsContainer = () => {
  const { showLoading, hideLoading } = useLoading()
  const [allShops, setAllShops] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_SHOPS)
  const [addShop, { error: errorAddShop }] = useMutation(ADD_SHOP, {
    update(cache, { data: { addShop: addShopItem } }) {
      const { shops } = cache.readQuery({ query: GET_SHOPS })
      cache.writeQuery({
        query: GET_SHOPS,
        data: { shops: shops.concat([addShopItem]) }
      })
      hideLoading()
      toast.success('Shop added')
    }
  })

  React.useEffect(() => {
    if (errorAddShop) {
      console.log(errorAddShop)
    }
  }, [errorAddShop])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllShops(data.shops)
    }
  }, [data, loading, error])

  const addShopHandler = value => {
    addShop({
      variables: {
        input: value
      }
    })
  }

  if (loading && !allShops) {
    return <Loading />
  }

  return (
    <Shops
      shops={allShops}
      addShop={addShop}
      refetch={refetch}
      addShopHandler={addShopHandler}
    />
  )
}

export default WithMainLayout(ShopsContainer)
